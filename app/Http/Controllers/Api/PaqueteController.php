<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Paquete;
use Illuminate\Http\Request;
use App\Http\Resources\PaqueteResource;
use App\Http\Resources\PaqueteActivoResource;
use Validator;

class PaqueteController extends Controller
{
    static function validateFieldsPaquete($input){
        $validator = Validator::make($input, [
        'paq_descripcion' => 'required|max:150',
        'paq_prod_id' => 'required',
        'paq_nro_unidades' => 'required',
        'paq_precio_venta' => 'required',
        'paq_neg_id'=> 'required'
        ]);
       
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    static function transformRequest(Request $request){
        $arrAux = [];
        foreach($request->all() as $key => $value){
            $newKey = "paq_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $idNegocio)
    {
        return PaqueteResource::collection(Paquete::where('paq_neg_id',$idNegocio)->take(100)->get());
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getActivos(int $idNegocio)
    {
        return PaqueteActivoResource::collection(Paquete::where('paq_neg_id',$idNegocio)
        ->where('paq_estado','A')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {            
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsPaquete($input);
            if(!is_null($validator)){
                return $validator;
            }
            $paqueteOld = Paquete::where('paq_descripcion',$request->descripcion)->count();
            if($paqueteOld <= 0){
                $paquete = Paquete::create(array_map('strtoupper',$input));
                return response()->json([
                "success" => true,
                "message" => "Paquete creado",
                // "data" => $paquete
                ],200);
            }else{    
                // throw new Exception(400, "Ya existe un paquete registrado con el usuario");
                return response()->json([
                    "error" => "Error 400",
                    "message" => "Ya existe un paquete registrado con la descripcion {$request->descripcion}",
                    // "data" => $paqueteOld
                    ],400);  
                // }                 
            }
        
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Paquete  $paquete
     * @return \Illuminate\Http\Response
     */
    public function show(Paquete $paquete)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Paquete  $paquete
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $idPaquete)
    {
        try { 
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsPaquete($input);
            if(!is_null($validator)){
                return $validator;
            }
            $paqueteOld = Paquete::find($idPaquete);
            if(!is_null($paqueteOld)){
                $updated = $paqueteOld->update(array_map('strtoupper',$input));  
                if($updated){       
                    return response()->json([
                        "success" => true,
                        "message" => "Paquete editado",
                        "data" => $paqueteOld
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
            
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Update the state of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Paquete  $paquete
     * @return \Illuminate\Http\Response
     */
    public function cambiarEstado(string $idPaquete)
    {
        try { 
            $paquete = Paquete::find($idPaquete);
            if(!is_null($paquete)){
                $estado = $paquete->paq_estado;
                if($estado == 'A'){
                    $paquete->paq_estado = 'I';
                }else{
                    $paquete->paq_estado = 'A';
                }                
                $saved=$paquete->save();
                if($saved){
                    return response()->json([
                        "success" => true,
                        // "message" => "Se cambio estado de Paquete",
                        "data" => $paquete
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],$e->getStatusCode() );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Paquete  $paquete
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paquete $paquete)
    {
        //
    }
}
