<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TransferenciaBancaria;
use Illuminate\Http\Request;
use App\Http\Resources\TransferenciaBancariaResource;
use Validator;
use Carbon\Carbon;

class TransferenciaBancariaController extends Controller
{
    public static function validateFields($input){
        $validator = Validator::make($input, [
        'trb_fp_id' => 'required',
        'trb_total_transferencia' => 'required',
        'trb_costo_transferencia' => 'required',
        'trb_total_transferido'=> 'required',
        'trb_neg_id'=> 'required',
        'trb_fecha_transferencia'=> 'nullable'
        ]);
       
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    public static function transformRequest(Request $request){
        $arrAux = [];
        foreach($request->all() as $key => $value){
            $newKey = "trb_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $idNegocio)
    {
        return TransferenciaBancariaResource::collection(TransferenciaBancaria::where('trb_neg_id',$idNegocio)
        ->where('trb_estado','A')
        ->orderBy('trb_fecha_transferencia','desc')
        ->take(100)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {            
            $input = $this->transformRequest($request);
            $validator = $this->validateFields($input);
            if(!is_null($validator)){
                return $validator;
            }else{
                $endDate = Carbon::now()->subSeconds(90);
                $TransferenciaOld = TransferenciaBancaria::where('trb_fp_id',$request->fp_id)
                ->where('trb_total_transferencia', $request->total_transferencia)
                ->where('trb_neg_id', $request->neg_id)
                ->where('trb_fecha_transferencia' ,'>=',$endDate)
                ->where('trb_estado','A')
                ->count();
                if($TransferenciaOld <= 0){
                    $transferencia = TransferenciaBancaria::create(array_map('strtoupper',$input));
                    return response()->json([
                    "success" => true,
                    "message" => "Transferencia creada",
                    // "data" => $FormaPago
                    ],200);
                }else{    
                    // throw new Exception(400, "Ya existe un FormaPago registrado con el usuario");
                    return response()->json([
                        "error" => "Error 400",
                        "message" => "Ya existe una Transferencia con los mismos datos hace menos de dos minuto.",
                        // "data" => $FormaPagoOld
                        ],400);  
                    // }                 
                }        
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idNegocio' => 'required',
            'fechaInicio' => 'required',
            'fechaFin'=> 'required'
        ]);

        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else {
            $fechaActual =date("Y-m-d H:i:s");
            $idNegocio= $request->input('idNegocio');
            $fechaFin = is_null($request->input('fechaFin')) ? $fechaActual: $request->input('fechaFin');
            $fechaInicio = is_null($request->input('fechaInicio')) ? date("d-m-Y",strtotime($fechaActual."- 1 month")): $request->input('fechaInicio');;

            return TransferenciaBancariaResource::collection(TransferenciaBancaria::where('trb_neg_id',$idNegocio)
            ->where('trb_estado','A')
            ->whereBetween('trb_fecha_transferencia', [$fechaInicio, $fechaFin])->orderBy('trb_fecha_transferencia','desc')->take(100)->get());
        }
    }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  \App\Models\FormaPago  $FormaPago
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, string $idFormaPago)
    // {
    //     try { 
    //         $input = $this->transformRequest($request);
    //         $validator = $this->validateFieldsFormaPago($input);
    //         if(!is_null($validator)){
    //             return $validator;
    //         }
    //         $FormaPagoOld = FormaPago::find($idFormaPago);
    //         if(!is_null($FormaPagoOld)){
    //             $updated = $FormaPagoOld->update(array_map('strtoupper',$input));  
    //             if($updated){       
    //                 return response()->json([
    //                     "success" => true,
    //                     "message" => "Forma Pago editada",
    //                     "data" => $FormaPagoOld
    //                 ],201);
    //             }else {
    //                 return response()->json(['error' => 'No guardado'], 500);
    //             }
    //         }else{
    //             return response()->json(['error' => 'No existe'], 404); 
    //         }
            
    //     } catch (Exception $e) {
    //         return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
    //     }
    // }

    /**
     * Update the state of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FormaPago  $FormaPago
     * @return \Illuminate\Http\Response
     */
    public function cambiarEstado(int $id)
    {
        try { 
            $transferencia = TransferenciaBancaria::find($id);
            if(!is_null($transferencia)){
                $estado = $transferencia->trb_estado;
                if($estado == 'A'){
                    $transferencia->trb_estado = 'I';
                }else{
                    $transferencia->trb_estado = 'A';
                }                
                $saved=$transferencia->save();
                if($saved){
                    return response()->json([
                        "success" => true,
                        // "message" => "Se cambio estado de FormaPago",
                        "data" => $transferencia
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],$e->getStatusCode() );
        }
    }
}
