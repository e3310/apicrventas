<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\VentaCab;
use App\Models\VentaDet;
use App\Models\Compra;
use App\Models\KardexCab;
use App\Models\KardexDet;
use App\Models\KardexLogic;
use App\Models\Producto;
use App\Models\Paquete;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Resources\VentaCabResource;
use Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\DB;

class VentaController extends Controller
{
    // Parametros search
    private $fieldFechas = "F";
    private $fieldPaquete = "P";
    private $fieldCliente = "C";

    function validateFieldsCab($input){
        $validator = Validator::make($input, [
        'ven_cli_id' => 'required',
        'ven_vend_id' => 'required',
        'ven_tfp_id' => 'required',
        'ven_fp_id' => 'nullable',        
        'ven_total' => 'nullable',
        'ven_neg_id' => 'required',
        'ven_kar_id' => 'required'
        ]);
       
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    function validateFieldsDet($input){
        $validator = Validator::make($input, [
        // 'vd_ven_id' => 'required',
        'vd_paq_id' => 'required',
        'vd_cantidad' => 'required',
        'vd_bonificacion' => 'required',
        'vd_total_det' => 'nullable'
        ]);
       
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    public static function transformRequest(Request $request){
        $arrAux = [];
        foreach($request->all() as $key => $value){
            $newKey = "ven_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }

    static function transformRequestDetalles($array, $prefijo){
        $aux= [];
        foreach ($array as $detalle) {
            $arrAux = [];
            foreach($detalle as $key => $value){
                $newKey = $prefijo.preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
                $arrAux[$newKey] = strtoupper($value);
            }
            array_push($aux, $arrAux);
        }
        return $aux;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $idNegocio)
    {
        return VentaCabResource::collection(
        VentaCab::join("tbl_v_vendedor","tbl_ventacab.ven_vend_id", '=', "tbl_v_vendedor.vend_id")
        ->join("tbl_persona", "tbl_v_vendedor.vend_per_id", '=',"tbl_persona.per_id")
        ->join("tbl_cliente", "tbl_ventacab.ven_cli_id", '=', "tbl_cliente.cli_id")
        ->join("tbl_ventadet", 'tbl_ventacab.ven_id', '=', 'tbl_ventadet.vd_ven_id')
        ->join("tbl_paquete", "tbl_ventadet.vd_paq_id", '=', "tbl_paquete.paq_id" )       
        ->where('ven_neg_id', $idNegocio)
        ->orderBy('ven_fecha_registro','desc')
        ->select('tbl_ventacab.*', 'vd_paq_id', 'vd_cantidad', 'vd_total_det', 'vd_bonificacion',
        'paq_descripcion', 'per_nombres','per_apellidos', 'cli_usuario_credito')
        ->take(100)->get());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idNegocio' => 'required',
            'value1' => 'required',
            'value2' => 'required',
            'field'=> 'required'
        ]);

        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else {

            $idNegocio= $request->input('idNegocio');
            $parametroSearch1= strtoupper($request->input('value1'));
            $parametroSearch2= strtoupper($request->input('value2'));
            $fieldSearch = $request->input('field'); //F-> fechas, P->paquetes, C->clientes
            return VentaCabResource::collection(
                VentaCab::join("tbl_v_vendedor","tbl_ventacab.ven_vend_id", '=', "tbl_v_vendedor.vend_id")
                ->join("tbl_persona", "tbl_v_vendedor.vend_per_id", '=',"tbl_persona.per_id")
                ->join("tbl_cliente", "tbl_ventacab.ven_cli_id", '=', "tbl_cliente.cli_id")
                ->join("tbl_ventadet", 'tbl_ventacab.ven_id', '=', 'tbl_ventadet.vd_ven_id')
                ->join("tbl_paquete", "tbl_ventadet.vd_paq_id", '=', "tbl_paquete.paq_id" )       
                ->where('ven_neg_id', $idNegocio)
            ->where(function($query) use ($parametroSearch1,$parametroSearch2, $fieldSearch) {
                switch($fieldSearch){
                    case($this->fieldFechas):
                        $starDate = Carbon::createFromFormat('Y-m-d H:i:s', $parametroSearch1." "."00:00:00", 'Europe/London');
                        $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $parametroSearch2." "."23:59:59", 'Europe/London');
                        $stringInicio = $starDate->format('Y-m-d H:i:s');
                        $stringFin = $endDate->format('Y-m-d H:i:s');
                        $query->whereBetween('ven_fecha_registro', [$stringInicio, $stringFin]);
                        break;
                    case($this->fieldPaquete):
                        $query->where('vd_paq_id', $parametroSearch1);
                        break;
                    case($this->fieldCliente):
                        $query->where('ven_cli_id', $parametroSearch1);
                        break;
                }
            })->orderBy('ven_fecha_registro','desc')
            ->select('tbl_ventacab.*', 'vd_paq_id', 'vd_cantidad', 'vd_total_det', 'vd_bonificacion',
                'paq_descripcion', 'per_nombres','per_apellidos', 'cli_usuario_credito')->get());

            
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {  
            $return ="INICIO";
            $kardexLogic = new KardexLogic();
            $verificarStock = $kardexLogic->verifyStock($request->cantidadVenta,$request->neg_id);
            if($verificarStock["available"] === false){
                $return = response()->json([
                                    "success" => false,
                                    "message" => "No existe stock",
                                    "stock" => $verificarStock["stock"]
                                    ],400);
            }else{ 
                DB::transaction(function () use ($request,$kardexLogic, &$return) {
                $input = $this->transformRequest($request);
                $endDate = Carbon::now()->subSeconds(90);

                $validator = $this->validateFieldsCab($input);
                if(!is_null($validator)){
                    $return = $validator;
                }else{
                    $ventaCabOld = VentaCab::where('ven_cli_id',$request->cli_id)
                    ->where('ven_vend_id', $request->vend_id)
                    ->where('ven_fecha_registro', '>=',$endDate)->count();
                    if($ventaCabOld <= 0){
                        $kardexCab = $kardexLogic->createKardexCab("V", $request->neg_id);
                        // if($kardexCab->exists){
                            $input["ven_kar_id"] = $kardexCab->kar_id;
                            $ventaCab = VentaCab::create($input);                           
                            //CONSULTA PRODUCTO
                            $producto = $kardexLogic->getProductoCredito();                      
                            //DETALLES
                            $detalles = $this->transformRequestDetalles($request->detalles,"vd_");
                            $detallesObject = [];
                            $kardexDetObject = [];
                            foreach ($detalles as $detalle) {
                                $detailObject = new VentaDet($detalle);
                                //CONSULTA PAQUETE
                                $paquete = $kardexLogic->getPaquete($detailObject->vd_paq_id);
                                //detalles venta
                                $detailObject->vd_total_det = $detailObject->vd_cantidad * $paquete->paq_precio_venta; 
                                $detallesObject[] = $detailObject;
                                //detalles kardex
                                $cantidadKardex = ($paquete->paq_nro_unidades*$detailObject->vd_cantidad) + $detailObject->vd_bonificacion;
                                $stockNuevo = $producto->pro_stock -($cantidadKardex);
                                $kardexDetObject[] = $kardexLogic->createKardexDet($kardexCab->kar_id,$producto,
                                $cantidadKardex,$stockNuevo,"V");
                                $kardexLogic->updateStockCostoProducto($producto->pro_id, $kardexCab->kar_neg_id, $stockNuevo);
                            }
                            
                            $ventaCab->ventaDets()->saveMany($detallesObject);
                            $kardexCab->kardexDets()->saveMany($kardexDetObject);

                            $return = response()->json([
                            "success" => true,
                            "message" => "Cabecera creada"
                            ],200);
                        // }
                    }else{    
                        $return = response()->json([
                            "error" => "Error",
                            "message" => "Se creo una venta con los mismos datos hace menos de dos minutos.",
                            ],409);               
                    }
                }
            },2);
            return $return;
        }        
    }catch (Exception $e) {
        return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
    }
}

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VentaCab  $ventaCab
     * @return \Illuminate\Http\Response
     */
    public function show(VentaCab $ventaCab)
    {
        //
    }

/**
     * Update the state of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cliente  $venta
     * @return \Illuminate\Http\Response
     */
    public function cambiarEstado(string $idVenta)
    {
        try {   
            $return ="INICIO";
                DB::transaction(function () use ($idVenta, &$return) {
                    $kardexLogic = new KardexLogic();
                    $venta = VentaCab::find($idVenta);
                    if(!is_null($venta) && $kardexLogic->verifyDateChangeStatus($venta->ven_fecha_registro) === true){
                        $estado = $venta->ven_estado;
                        if($estado == 'A'){
                            $venta->ven_estado = 'I';
                        }else{
                            $venta->ven_estado = 'A';
                        }        
                        $saved=$venta->save();  
                        $savedKardex = $kardexLogic->changeStatusKardex($venta->ven_kar_id,$venta->ven_estado);         

                        if($saved && $savedKardex){
                            //KARDEX CAB
                            $idAnteriorKardex = $kardexLogic->getIdKardexPrev($venta->ven_kar_id, $venta->ven_neg_id);
                            // $idAnteriorKardex= KardexCab::where('kar_id','<',$venta->ven_kar_id)
                            // ->where('kar_estado','A')
                            // ->orderBy('kar_fecha_registro','desc')->first();   
                            $rowsError = $kardexLogic->getRowsKardexNext($idAnteriorKardex,$venta->ven_neg_id);
                            // $rowsError = KardexCab::where('kar_id','>=',$idAnteriorKardex)
                            // ->where('kar_estado','A');
                            $movsRegularizar = $kardexLogic->getMovsKardexNext($rowsError);
                            //KARDEX DET
                            $idsRegularizar = $kardexLogic->getIdsMovsKardex($movsRegularizar);
                            // $idsRegularizar = $rowsError->get()->map(function ($item) {
                            //     return $item->kar_id;
                            // });
                            // $movsRegularizar = $rowsError->get();
                            $detRegularizar = $kardexLogic->getDetKardexByIdArray($idsRegularizar);
                            // $detRegularizar = KardexDet::whereIn('kd_kar_id', $idsRegularizar)
                            //         ->orderBy('kd_id')->get();

                            //COMPRAS
                            $movsCompras = $kardexLogic->getKardexOperation($rowsError, 'C');
                            // $movsCompras = $rowsError->where('kar_tipo_mov','C')->get();
                            $idsKarCompras = $kardexLogic->getIdsMovsKardex($movsCompras);
                            // $idsKarCompras = $movsCompras->map(function ($item) {
                            //     return $item->kar_id;
                            // });
                            $comprasCosto = $kardexLogic->getComprasCostoByArray($idsKarCompras,$venta->ven_neg_id);
                            // $comprasCosto = Compra::whereIn('com_kar_id', $idsKarCompras)
                            // ->where('com_estado','A')
                            // ->select('com_id','com_cantidad','com_costo','com_kar_id')->get();               

                            $kardexLogic->regularizarStock($movsRegularizar,$detRegularizar,$comprasCosto);
                            // $index =0;
                            // $updated ='a';
                            // $valPrueba =[];
                            
                            // foreach($detRegularizar as $det){
                            //     if($index>0){
                            //         $anteriorKardex = $detRegularizar[$index-1];                        
                            //         $actualKardex = $detRegularizar[$index];
                            //         $costoNuevo= $anteriorKardex->kd_costo;
                            //         $stockNuevo = $anteriorKardex->kd_stock + $actualKardex->kd_cantidad;
                            //         $cab = $movsRegularizar->where('kar_id', $det->kd_kar_id)->first();
                            //         if(strcmp($cab["kar_tipo_mov"], "C") === 0){
                            //             $buscarCompra= $comprasCosto->where('com_kar_id',$det->kd_kar_id)->first();
                            //             $costoNuevo= round((
                            //                 ($anteriorKardex->kd_cantidad * $anteriorKardex->kd_costo) + ($actualKardex->kd_cantidad * $buscarCompra->com_costo))
                            //                 /(abs($anteriorKardex->kd_cantidad)+abs($actualKardex->kd_cantidad)),2);
                                        
                            //         }
                            //         $totalDetalle = round($actualKardex->kd_cantidad * $costoNuevo,2);
                            //         $updated = KardexDet::where('kd_kar_id',$det->kd_kar_id)
                            //         ->update(['kd_stock' => $stockNuevo,
                            //             'kd_costo'=> $costoNuevo,
                            //             'kd_total_detalle'=> round(($det->kd_cantidad * $costoNuevo),2)]);
                            //         //Modificando coleccion
                            //         $det->kd_stock = $stockNuevo;
                            //         $det->kd_costo= $costoNuevo;                         
                            //     }
                            //     $index++;
                            // }
                            $ultimoKardex= $detRegularizar[$detRegularizar->count()-1];
                            $kardexLogic->updateStockCostoProducto($ultimoKardex->kd_pro_id,
                            $venta->ven_neg_id, $ultimoKardex->kd_stock, $ultimoKardex->kd_costo);
                            // $updateProducto = Producto::where('pro_nombre','CREDITO')
                            // ->where('pro_neg_id',$venta->ven_neg_id)
                            // ->update(['pro_costo' => $ultimoKardex->kd_costo,
                            // 'pro_stock'=> $ultimoKardex->kd_stock]);
                            $return = response()->json([
                                "success" => true,
                            ],201);
                        }else{
                            $return = response()->json(['error' => 'No actualizado'], 304);
                        }
                }else{
                    $return= response()->json(['error' => 'No existe'], 403); 
                }
            });
            return $return;
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],$e->getStatusCode() );
        }
    }

}
