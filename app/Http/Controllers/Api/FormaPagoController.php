<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\FormaPago;
use Illuminate\Http\Request;
use App\Http\Resources\FormaPagoResource;
use App\Http\Resources\FormaPagoActivoResource;
use Validator;

class FormaPagoController extends Controller
{
    public static function validateFieldsFormaPago($input){
        $validator = Validator::make($input, [
        'fp_numero' => 'required|max:40',
        'fp_tipo_cuenta' => 'nullable|max:1',
        'fp_entidad_bancaria' => 'required|max:200',
        'fp_nombres_titular'=> 'required|max:100',
        'fp_apellidos_titular'=> 'required|max:100',
        'fp_ci_titular'=> 'required|max:15',
        'fp_telefono_titular'=> 'nullable|max:15',
        'fp_email_titular'=> 'nullable|max:45',
        'fp_neg_id'=> 'required',
        'fp_apellidos_titular'=> 'required|max:100',
        ]);
       
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    public static function transformRequest(Request $request){
        $arrAux = [];
        foreach($request->all() as $key => $value){
            $newKey = "fp_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $idNegocio)
    {
        return FormaPagoResource::collection(FormaPago::where('fp_neg_id',$idNegocio)->take(100)->get());
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getActivos(int $idNegocio, int $idTipo)
    {
        return FormaPagoActivoResource::collection(FormaPago::where('fp_neg_id',$idNegocio)
        ->where('fp_tipofp', $idTipo)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {            
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsFormaPago($input);
            if(!is_null($validator)){
                return $validator;
            }
            $FormaPagoOld = FormaPago::where('fp_numero',$request->numero)->count();
            if($FormaPagoOld <= 0){
                $FormaPago = FormaPago::create(array_map('strtoupper',$input));
                return response()->json([
                "success" => true,
                "message" => "FormaPago creada",
                // "data" => $FormaPago
                ],200);
            }else{    
                // throw new Exception(400, "Ya existe un FormaPago registrado con el usuario");
                return response()->json([
                    "error" => "Error 400",
                    "message" => "Ya existe una Forma Pago registrada con el numero {$request->numero}",
                    // "data" => $FormaPagoOld
                    ],400);  
                // }                 
            }        
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FormaPago  $FormaPago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $idFormaPago)
    {
        try { 
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsFormaPago($input);
            if(!is_null($validator)){
                return $validator;
            }
            $FormaPagoOld = FormaPago::find($idFormaPago);
            if(!is_null($FormaPagoOld)){
                $updated = $FormaPagoOld->update(array_map('strtoupper',$input));  
                if($updated){       
                    return response()->json([
                        "success" => true,
                        "message" => "Forma Pago editada",
                        "data" => $FormaPagoOld
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
            
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Update the state of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FormaPago  $FormaPago
     * @return \Illuminate\Http\Response
     */
    public function cambiarEstado(string $idFormaPago)
    {
        try { 
            $FormaPago = FormaPago::find($idFormaPago);
            if(!is_null($FormaPago)){
                $estado = $FormaPago->fp_estado;
                if($estado == 'A'){
                    $FormaPago->fp_estado = 'I';
                }else{
                    $FormaPago->fp_estado = 'A';
                }                
                $saved=$FormaPago->save();
                if($saved){
                    return response()->json([
                        "success" => true,
                        // "message" => "Se cambio estado de FormaPago",
                        "data" => $FormaPago
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],$e->getStatusCode() );
        }
    }
}
