<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Usuario;
use App\Models\LoginGrants;
use App\Models\PersonalAccessToken;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Http\Resources\LoginResource;

class LoginController extends Controller
{

  // Metodo que verifica que llegue la informacion correctamente
  private function validateLogin(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'username' => 'required',
      'password' => 'required',
      'device' => 'required'
    ]);
  
    if($validator->fails()){
        return response()-> json([
            'message' => 'Validation Error.',
            'error' => $validator->errors()
            ],500);               
    }else return null;
  }
  
  private function loadInfoLogin(Request $request){
    $permisosCollection = 
    LoginResource::collection(
        Usuario
        ::join('tbl_tipousuario', 'tbl_usuario.us_tipo_id', '=', 'tbl_tipousuario.tipo_id')
        ->join('tbl_seg_rel_permisopagina', 'tbl_tipousuario.tipo_id', '=','tbl_seg_rel_permisopagina.srpp_tipo_us')
        ->join('tbl_seg_rel_accion_pagina','tbl_seg_rel_permisopagina.srpp_accion_pag_id','=','tbl_seg_rel_accion_pagina.srap_pag_id')
        ->join('tbl_seg_pagina', 'tbl_seg_rel_accion_pagina.srap_pag_id','=', 'tbl_seg_pagina.spag_id')
        ->join('tbl_seg_accionmodelo','tbl_seg_rel_accion_pagina.srap_accion_id','=', 'tbl_seg_accionmodelo.sam_id')
        ->where('us_usuario', $request->username)
        ->where('us_estado','A')
        ->where('srpp_estado','A')
        ->where('srap_estado','A')
        ->select('us_id','us_usuario','us_tipo_id','us_neg_id','tipo_nombre',
        'spag_nombre','spag_abr','spag_ruta','spag_icono_fs',
        'sam_nombre')->get()
    );

    $permisos = null;
    $usuarioInfo = null;
    $paginas = null;

    if($permisosCollection->count()>0){
      $firstItem = $permisosCollection->first();
      $usuarioInfo = [
        'userKey' => $firstItem->us_id,
        'userName' => $firstItem->us_usuario,
        'typeKey' => $firstItem->us_tipo_id,
        'typeName' => $firstItem->tipo_nombre,
        'idNegocio' => $firstItem->us_neg_id
      ];
      $grupo = $permisosCollection->map(function ($item) {
        return [ 
          'abr' => $item['spag_abr'],
          'page' => $item['spag_nombre'],
          'url' => $item['spag_ruta'],
          'icon' => $item['spag_icono_fs']
        ];
      });
      
      $paginas = $grupo->unique()->values()->all();

      $permisos = $permisosCollection->mapToGroups(function ($item, $key) {
        $accion ='';
        switch ($item['sam_nombre']) {
          case 'LIST':
            $accion = 'R';
            break;
          case 'STATE':
            $accion ='D';
            break;
          default:
            $accion = substr($item['sam_nombre'], 0, 1);
        }
        return [$item['spag_abr'] => $accion];
      })->map(function ($item, $key) {
            return implode("",$item->toArray());
        });

    //   $permisos = $actionsPaginas->map(function ($item, $key) {
    //     return ["n"=>$key,"a"=>$item];
    // });

      

      return 
        ['usuario' => $usuarioInfo,
        'permisos'=> $permisos->toArray(),
        'paginas'=> $paginas];
    }else{
      return null;
    }
  }



  public function login(Request $request)
  {
    $validator = $this->validateLogin($request);
    if(!is_null($validator)){
        return $validator;
    }else{
      $user = Usuario::where('us_usuario', $request->username)->first();
      if(strcmp($user->us_estado,'A') === 0){
        $password = '';
        $decrypted = '';
        try {
          $decrypted = Crypt::decryptString($user->us_clave);
        } catch (DecryptException $e) {
            
        }
        $password =$decrypted;
        if (! $user || strcmp($password, $request->password) != 0 ) { //! Hash::check($request->password, $user->password)
          return response()->json([
                'message'=> 'Unauthorized'
            ],400);
        }else{
          $token = PersonalAccessToken::where('tokenable_id', $user->us_id)->delete();
          $infoLogin = $this->loadInfoLogin($request);
          if(is_null($infoLogin)){
            return response()->json([
              'message'=>'Ungranted'
            ],404);
          }else{
            $crearToken = $user->createToken($request->device);
            return response()->json([
              'userInfo' => $infoLogin['usuario'],
              'token' => $crearToken->plainTextToken,
              'tokenCreation' => $crearToken->accessToken->created_at,
              'grants' => $infoLogin['permisos'],
              'menu' => $infoLogin['paginas'],
              'status'=> 200
            ],200);
          }
        }
      }else if(strcmp($user->us_estado,'P') === 0){
        return response()->json([
          'message'=>'Pending'
        ],401);
      }else if(strcmp($user->us_estado,'I') === 0){
        return response()->json([
          'message'=>'Desactivated'
        ],403);        
      }
  }
  }

  private function validateLogout(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'userkey' => 'required'
      // ,
      // 'password' => 'required',
      // 'device' => 'required'
    ]);
  
    if($validator->fails()){
        return response()-> json([
            'message' => 'Validation Error.',
            'error' => $validator->errors()
            ],500);               
    }else return null;
  }

  public function logout(Request $request){
    $validator = $this->validateLogout($request);
    if(!is_null($validator)){
        return $validator;
    }
    $user = Usuario::where('us_id', $request->userkey)->first();
    if (! $user) { //! Hash::check($request->password, $user->password)
      return response()->json([
            'message'=> 'Not Found'
        ],404);
    }else{
      $token = PersonalAccessToken::where('tokenable_id', $request->userkey)->delete();
      // ->orderBy('created_at','desc')->take(1)->delete();
      if($token>0){
        return response()->json(['message' => 'User successfully signed out'],201);
      }else{
        return response()->json(['message' => 'Not Found tokens to delete'],404);
      }
      
    }
    
  }
}
