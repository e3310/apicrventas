<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Compra;
use Illuminate\Http\Request;
use App\Http\Resources\CompraResource;
use App\Models\KardexLogic;
use Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CompraController extends Controller
{
    public static function validateFieldsCompra($input){
        $validator = Validator::make($input, [
        // 'com_pro_id' => 'required',
        'com_cantidad' => 'required',
        'com_costo' => 'required',
        'com_total' => 'required',
        'com_neg_id' => 'required',
        // 'com_kar_id' => 'required'
        ]);
       
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    public static function transformRequest(Request $request){
        $arrAux = [];
        foreach($request->all() as $key => $value){
            $newKey = "com_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $idNegocio)
    {
        return CompraResource::collection(Compra::where('com_neg_id',$idNegocio)
        ->orderBy('com_fecha_registro','desc')->take(100)->get());
    }

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idNegocio' => 'required',
            'fechaInicio' => 'required',
            'fechaFin'=> 'required'
        ]);

        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else {
            $fechaActual =date("Y-m-d H:i:s");
            $idNegocio= $request->input('idNegocio');
            $fechaFin = is_null($request->input('fechaFin')) ? $fechaActual: $request->input('fechaFin');
            $fechaInicio = is_null($request->input('fechaInicio')) ? date("d-m-Y",strtotime($fechaActual."- 1 month")): $request->input('fechaInicio');;

            return CompraResource::collection(Compra::where('com_neg_id',$idNegocio)
            ->whereBetween('com_fecha_registro', [$fechaInicio, $fechaFin])->orderBy('com_fecha_registro','desc')->take(100)->get());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {   
            $return ="INICIO";  
            DB::transaction(function () use ($request, &$return) {       
                $input = $this->transformRequest($request);
                $endDate = Carbon::now()->subSeconds(90);

                $kardexLogic = new KardexLogic();

                $validator = $this->validateFieldsCompra($input);
                if(!is_null($validator)){
                    $return= $validator;
                }else{
                    $compraOld = Compra::where('com_cantidad',$request->cantidad)
                    ->where('com_costo', $request->costo)
                    ->where('com_neg_id', $request->neg_id)
                    ->where('com_fecha_registro', '>=',$endDate)->count();
                    if($compraOld <= 0){
                        //CONSULTA PRODUCTO
                        $producto = $kardexLogic->getProductoNegocio($request->neg_id); 
                        $kardexCab = $kardexLogic->createKardexCab("C", $request->neg_id);
                        $input["com_kar_id"] = $kardexCab->kar_id;
                        $input["com_pro_id"] = $producto->pro_id;
                        // $input+=["com_total" => round(abs($request->costo)*abs($request->cantidad),2)];
                        $compra = Compra::create($input);                        
                        $kardexDetObject = [];
                        $cantidadKardex = $compra->com_cantidad;
                        $stockNuevo = $producto->pro_stock +$cantidadKardex;
                        $kardexDetObject[] = $kardexLogic->createKardexDet($kardexCab->kar_id,$producto,
                            $cantidadKardex,$stockNuevo,"C",$compra->com_costo);
                        $kardexLogic->updateStockCostoProducto($producto->pro_id, $kardexCab->kar_neg_id, $stockNuevo);

                        $kardexCab->kardexDets()->saveMany($kardexDetObject);
                        
                        $return= response()->json([
                        "success" => true,
                        "message" => "Compra creada",
                        // "data" => $compra
                        ],200);
                    }else{    
                        // throw new Exception(400, "Ya existe un compra registrado con el usuario");
                        $return= response()->json([
                            "error" => "Error",
                            "message" => "Ya existe una compra registrada con el id {$request->id}",
                            // "data" => $compraOld
                            ],409);  
                        // }                 
                    }
                }     
            });
            return $return;   
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function show(Compra $compra)
    {
    }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  \App\Models\Compra  $compra
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, string $idCompra)
    // {
    //     try { 
    //         $input = $this->transformRequest($request);
    //         $validator = $this->validateFieldsCompra($input);
    //         if(!is_null($validator)){
    //             return $validator;
    //         }
    //         $compraOld = Compra::find($idCompra);
    //         if(!is_null($compraOld)){
    //             $updated = $compraOld->update(array_map('strtoupper',$input));  
    //             if($updated){       
    //                 return response()->json([
    //                     "success" => true,
    //                     "message" => "Compra editada",
    //                     "data" => $compraOld
    //                 ],201);
    //             }else {
    //                 return response()->json(['error' => 'No guardado'], 500);
    //             }
    //         }else{
    //             return response()->json(['error' => 'No existe'], 404); 
    //         }
            
    //     } catch (Exception $e) {
    //         return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
    //     }
    // }

    /**
     * Update the state of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function cambiarEstado(string $idCompra)
    {
        try { 
            $return ="INICIO";
            DB::transaction(function () use ($idCompra, &$return) {
                $kardexLogic = new KardexLogic();
                $compra = Compra::find($idCompra);
                if(!is_null($compra) && $kardexLogic->verifyDateChangeStatus($compra->com_fecha_registro) === true){
                    $estado = $compra->com_estado;
                    if($estado == 'A'){
                        $compra->com_estado = 'I';
                    }else{
                        $compra->com_estado = 'A';
                    }                
                    $saved=$compra->save();
                    $savedKardex = $kardexLogic->changeStatusKardex($compra->com_kar_id,$compra->com_estado);         

                    if($saved && $savedKardex){
                        $idAnteriorKardex = $kardexLogic->getIdKardexPrev($compra->com_kar_id, $compra->com_neg_id);
                        $rowsError = $kardexLogic->getRowsKardexNext($idAnteriorKardex,$compra->com_neg_id);
                        $movsRegularizar = $kardexLogic->getMovsKardexNext($rowsError);
                        $idsRegularizar = $kardexLogic->getIdsMovsKardex($movsRegularizar);
                        $detRegularizar = $kardexLogic->getDetKardexByIdArray($idsRegularizar);
                        $movsCompras = $kardexLogic->getKardexOperation($rowsError, 'C');
                        $idsKarCompras = $kardexLogic->getIdsMovsKardex($movsCompras);
                        $comprasCosto = $kardexLogic->getComprasCostoByArray($idsKarCompras,$compra->com_neg_id);
                        $kardexLogic->regularizarStock($movsRegularizar,$detRegularizar,$comprasCosto);
                        $ultimoKardex= $detRegularizar[$detRegularizar->count()-1];
                        $kardexLogic->updateStockCostoProducto($ultimoKardex->kd_pro_id,
                            $compra->com_neg_id, $ultimoKardex->kd_stock, $ultimoKardex->kd_costo);
                        $return = response()->json([
                            "success" => true
                            // "dato" => sRegularizar,
                            // "data" => $detRegularizar,
                            // "data0"=>$movsCompras,
                            // "data1"=> $idsKarCompras,
                            // "data2"=> $comprasCosto
                        ],201);
                    }else {
                        $return = response()->json(['error' => 'No actualizado'], 309);
                    }
                }else{
                    $return = response()->json(['error' => 'Fecha no válida'], 403); 
                }
            });
            return $return;
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],$e->getStatusCode() );
        }
    }
}
