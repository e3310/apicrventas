<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Persona;
use Illuminate\Http\Request;
use Validator;

class PersonaController extends Controller
{

    static function validateFieldsPersona($input){
        $validator = Validator::make($input, [
            'per_cedula' => 'required|max:10',
            'per_nombres' => 'required|min:2',
            'per_apellidos' => 'required|min:2',          
        ]);
        
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    static function transformRequest(Request $request){
        $arrAux = [];
        foreach($request->all() as $key => $value){
            $newKey = "per_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $idPersona)
    {
        try { 
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsPersona($input);
            if(!is_null($validator)){
                return $validator;
            }
            $personaOld = Persona::find($idPersona);
            if(!is_null($personaOld)){
                $updated = $personaOld->update(array_map('strtoupper',$input));  
                if($updated){       
                    return response()->json([
                        "success" => true,
                        "message" => "Persona editado",
                        "data" => $personaOld
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
            
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }
    
}
