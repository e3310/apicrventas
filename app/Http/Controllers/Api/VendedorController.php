<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Vendedor;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\VendedorActivoResource;

class VendedorController extends Controller
{

    public function getActivos(int $idNegocio)
    {
        return 
        VendedorActivoResource::collection(
            Usuario::join('tbl_tipousuario', 'tbl_usuario.us_tipo_id', '=', 'tbl_tipousuario.tipo_id')
        ->join('tbl_persona', 'us_per_id', 'tbl_persona.per_id')
        // ->join('tbl_v_vendedor','per_id', 'tbl_v_vendedor.vend_per_id')
        ->join('tbl_v_vendedor', function ($join) {
            $join->on('per_id', '=', 'tbl_v_vendedor.vend_per_id')
                 ->where('tbl_v_vendedor.vend_estado', '=', 'A');
        })
        ->where('tbl_usuario.us_neg_id',$idNegocio)
        ->where('tbl_usuario.us_estado','A')
        ->select('per_nombres','per_apellidos','vend_id')
        ->orderBy('per_apellidos','desc')->get()
        );
    }

    
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendedor  $vendedor
     * @return \Illuminate\Http\Response
     */
    public function show(int $idUsuario)
    {
        return 
        // VendedorActivoResource::collection(
            Usuario::join('tbl_tipousuario', 'tbl_usuario.us_tipo_id', '=', 'tbl_tipousuario.tipo_id')
        ->join('tbl_persona', 'us_per_id', 'tbl_persona.per_id')
        ->join('tbl_v_vendedor', function ($join) {
            $join->on('per_id', '=', 'tbl_v_vendedor.vend_per_id')
                 ->where('tbl_v_vendedor.vend_estado', '=', 'A');
        })
        ->where('tbl_usuario.us_id',$idUsuario)
        ->where('tbl_usuario.us_estado','A')
        ->select('per_nombres as nombres','per_apellidos as apellidos','vend_id as idVendor')
        ->orderBy('per_apellidos','desc')->first();
        // );
    }

    public function cambiarEstado(string $idPersona)
    {
        try {          
                $return ="prueba";
                DB::transaction(function () use ($idPersona, &$return) {
                    $return = "vendedor";                
                    $vendedor = Vendedor::where('vend_per_id',$idPersona)->first();
                    if(!is_null($vendedor)){
                        $usuario = Usuario::firstWhere('us_per_id',$idPersona);
                        $estado = $vendedor->vend_estado;
                        $estadoNuevo= null;
                        if($estado == 'A'){
                            $estadoNuevo = 'I';
                        }else{
                            $estadoNuevo = 'A';
                        }            
                        $vendedor->vend_estado = $estadoNuevo;
                        $usuario->us_estado = $estadoNuevo;
                        $savedVendedor = $vendedor->save();
                        $savedUsuario = $usuario->save();
                        
                        if($savedVendedor && $savedUsuario){
                            $return = response()->json([
                                "success" => true,
                                "message" => "Se cambio estado de Vendedor",
                                // "data" => $usuario
                            ],201);
                        }else {
                            $return = response()->json(['error' => 'No guardado'], 500);
                        }
                    }else{
                        $return = response()->json(['error' => 'No existe'], 404); 
                    }
                });
                return $return;
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],$e->getStatusCode() );
        }
        
    }
}
