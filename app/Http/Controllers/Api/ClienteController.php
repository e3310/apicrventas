<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\Pais;
use App\Models\CliVentaMes;
use App\Models\RepCliVentaCab;
use App\Models\RepClienteVentaLogic;
use App\Imports\UsersImport;
use Illuminate\Http\Request;
use App\Http\Resources\ClienteResource;
use App\Http\Resources\ClienteActivoResource;
use Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    private $fieldNombre = 'N';
    private $fieldApellido = 'A';
    private $fieldUsuarioCreditos = 'U';
    private $fieldPais ='P';

    public static function validateFieldsCliente($input){
        $validator = Validator::make($input, [
        // 'cli_id' => 'required',
        'cli_usuario_credito' => 'required|max:45',
        'cli_telefono' => 'required|max:15',
        'cli_pais_id' => 'required',
        'cli_neg_id' => 'required',        
        'cli_nombres' => 'max:100',
        'cli_apellidos' => 'max:100'
        ]);
       
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    public static function transformRequest(Request $request){
        $arrAux = [];
        foreach($request->all() as $key => $value){
            $newKey = "cli_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }


    static function transformRequestArraysMasivos($array, $prefijo){
        $aux= [];
        foreach ($array as $pais) {
            $arrAux = [];
            foreach($pais as $key => $value){
                $newKey = $prefijo.preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
                $arrAux[$newKey] = strtoupper($value);
            }
            array_push($aux, $arrAux);
        }
        return $aux;
    }

    /**
     * @param $array            Array con información de tabla actual
     * @param $arrayRelate      Array con información de tabla relacionada
     * @param $prefijoTabla     Prefijo de tabla actual
     * @param $campoFK          Campo de clave foranea en la tabla actual
     * @param $campoPKRelate    Campo de clave primaria de la tabla relacionada
     * @param $campoNameRelate  Campo de nombre único de la tabla relacionada   
     */
    static function transformRequestArraysMassRelate($array, $arrayRelate, $prefijoTabla, $campoFK, $campoPKRelate, $campoNameRelate){
        $clientesAux= [];
        foreach ($array as $pais) {
            $arrAux = [];
            foreach($pais as $key => $value){
                $newKey = $prefijoTabla.preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
                if($newKey === $campoFK){
                    $keyRelate = array_search(strtoupper($value), array_column($arrayRelate, $campoNameRelate));
                    $arrAux[$newKey] = $arrayRelate[$keyRelate][$campoPKRelate];
                }else{
                    $arrAux[$newKey] = strtoupper($value);
                }
            }
            array_push($clientesAux, $arrAux);
        }
        return $clientesAux;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $idNegocio)
    {
        return ClienteResource::collection(Cliente::join('tbl_pais', 'tbl_pais.pais_id', '=', 'tbl_cliente.cli_pais_id')
        ->where('cli_neg_id',$idNegocio)
        ->select('tbl_cliente.*', 'tbl_pais.pais_nombre')
        ->orderBy('cli_fecha_registro','desc')->take(100)->get());
        // ::where('cli_neg_id',$idNegocio)->orderBy('cli_fecha_registro','desc')->take(100)->get());
    }

    public function getActivos(int $idNegocio){
        return ClienteActivoResource::collection(Cliente::where('cli_neg_id',$idNegocio)
        ->where('cli_estado','A')
        ->select('cli_id','cli_nombres','cli_apellidos','cli_usuario_credito')
        ->orderBy('cli_usuario_credito')->get());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idNegocio' => 'required',
            'value' => 'required',
            'field'=> 'required'
        ]);

        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else {

            $idNegocio= $request->input('idNegocio');
            $parametroSearch= strtoupper($request->input('value'));
            $fieldSearch = $request->input('field'); //N-> nombres, A->apellidos, P->pais
            return ClienteResource::collection(Cliente::join('tbl_pais', 'tbl_pais.pais_id', '=', 'tbl_cliente.cli_pais_id')
            ->where('cli_neg_id',$idNegocio)
            ->where(function($query) use ($parametroSearch, $fieldSearch) {
                switch($fieldSearch){
                    case($this->fieldNombre):
                        $query->where('cli_nombres', 'like',"%{$parametroSearch}%");
                        break;
                    case($this->fieldApellido):
                        $query->where('cli_apellidos', 'like',"%{$parametroSearch}%");
                        break;
                    case($this->fieldUsuarioCreditos):
                        $query->where('cli_usuario_credito', 'like',"%{$parametroSearch}%");
                        break;
                    case($this->fieldPais):
                        $query->where('cli_pais_id',"{$parametroSearch}");
                        break;
                }
            })
            ->select('tbl_cliente.*', 'tbl_pais.pais_nombre')
            ->orderBy('cli_fecha_registro','desc')->take(100)->get());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {            
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsCliente($input);
            if(!is_null($validator)){
                return $validator;
            }
            // $clienteOld = Cliente::find($request->id);
            $clienteOld = Cliente::where('cli_usuario_credito',$request->usuario_credito)->count();
            if($clienteOld <= 0){
                $cliente = Cliente::create(array_map('strtoupper',$input));
                return response()->json([
                "success" => true,
                "message" => "Cliente creado",
                // "data" => $cliente
                ],200);
            }else{    
                // throw new Exception(400, "Ya existe un cliente registrado con el usuario");
                return response()->json([
                    "error" => "Error 400",
                    "message" => "Ya existe un cliente registrado con el usuario {$request->id}",
                    // "data" => $clienteOld
                    ],400);  
                // }                 
            }
        
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        return new  NegocioResource($negocio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $idCliente)
    {
        try { 
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsCliente($input);
            if(!is_null($validator)){
                return $validator;
            }
            $clienteOld = Cliente::find($idCliente);
            if(!is_null($clienteOld)){
                $updated = $clienteOld->update(array_map('strtoupper',$input));  
                if($updated){       
                    return response()->json([
                        "success" => true,
                        "message" => "Cliente editado",
                        "data" => $clienteOld
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
            
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Update the state of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function cambiarEstado(string $idCliente)
    {
        try { 
            $cliente = Cliente::find($idCliente);
            if(!is_null($cliente)){
                $estado = $cliente->cli_estado;
                if($estado == 'A'){
                    $cliente->cli_estado = 'I';
                }else{
                    $cliente->cli_estado = 'A';
                }                
                $saved=$cliente->save();
                if($saved){
                    return response()->json([
                        "success" => true,
                        // "message" => "Se cambio estado de Cliente",
                        "data" => $cliente
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],$e->getStatusCode() );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeMasivo(Request $request)
    {
        try {            
            // $return ="INICIO";
            DB::transaction(function () use ($request, &$return) {      
                // $reporteLogic = new RepClienteVentaLogic();
                //         $ventasInput = $this->transformRequestArraysMasivos($request->ventas, "rcvd_");
                //         $ventas = $reporteLogic-> createNuevoReporteFromArray($ventasInput);
                //             $return = response()->json([
                //                 "success" => true,
                //                 "message" => "OK",
                //                 "data" => $ventas
                //                 ],200);

                $paisesInput = $this->transformRequestArraysMasivos($request->paises, 'pais_');
                if(Pais::insert($paisesInput) === true){
                    $paisesNames = array_column($paisesInput, 'pais_nombre');
                    $idPaises= Pais::select('pais_id','pais_nombre')->whereIn('pais_nombre',$paisesNames)->get()->toArray();

                    // $array, $arrayRelate, $prefijoTabla, $campoFK, $campoPKRelate, $campoNameRelateKC
                    $clientesInput= $this->transformRequestArraysMassRelate($request->clientes, $idPaises, "cli_","cli_pais_id","pais_id","pais_nombre");

                    if(Cliente::insert($clientesInput) === true){
                        $usuariosCliente = array_column($clientesInput, 'cli_usuario_credito');
                        $idClientes= Cliente::select('cli_id','cli_usuario_credito')->whereIn('cli_usuario_credito',$usuariosCliente)->get()->toArray();

                        $ventasInput = $this->transformRequestArraysMassRelate($request->ventas, $idClientes, "rcvd_","rcvd_cli_id","cli_id","cli_usuario_credito");
                        $return = "Transformado";
                        $reporteLogic = new RepClienteVentaLogic();
                        $return= "iniciado";
                        // $transformado = $this->transformRequestArraysMasivos($request->ventas, "rcvd_");
                        $ventas = $reporteLogic-> createNuevoReporteFromArray($ventasInput);
$return = "ventas pasada por logic";
                        if(RepCliVentaCab::insert($ventas) === true){ 
                            $return = response()->json([
                                "success" => true,
                                "message" => "OK",
                                "data" => $ventas
                                ],200);
                        }else{
                            $return = response()->json([
                                "success" => false,
                                "message" => "Ventas no insertadas. Proceso finalizó con errores",
                                ],400);   
                        }
                    }else{
                        $return = response()->json([
                            "success" => false,
                            "message" => "Clientes no insertados. Proceso finalizó con errores",
                            ],400);    
                    }
                }else{
                    $return = response()->json([
                        "success" => false,
                        "message" => "Paises no insertados. Proceso finalizó con errores",
                        ],400);
                }
            });
            return $return;
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        //
    }
}
