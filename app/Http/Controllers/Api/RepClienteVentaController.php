<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\RepCliventaCab;
use App\Models\RepCliventaDet;
use App\Models\Cliente;
use App\Models\VentaCab;
use App\Models\VentaDet;
use App\Models\RepClienteVentaLogic;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use DB;
use App\Http\Resources\RepCliVentaCabResource;

class RepClienteVentaController extends Controller
{
    private $typeParcial ="P";
    private $typeMensual ="M";
    public static function validateFields($input){

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $idNegocio)
    {
        return RepCliVentaCabResource::collection(
            RepCliventaCab::where('rcvc_neg_id',$idNegocio)
            ->orderBy('rcvc_fecha_generado','desc')
            ->take(100)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {  
            $return ="INICIO";
            DB::transaction(function () use ($request, &$return) {
                $logic = new RepClienteVentaLogic();                
                $validator = Validator::make($request->all(), [
                    'idNegocio' => 'required',
                    'type' => 'required',
                    'year' => 'required',
                    'month' => 'required'
                    ]);
            
                if($validator->fails()){
                    $return= response()-> json([
                        'message' => 'Validation Error.',
                        'error' => $validator->errors()
                        ],500);               
                }else{                    
                    $idNegocio= $request->input('idNegocio');
                    $tipo= $request->input('type');
                    $anio = $request->input('year');    
                    $mes = $request->input('month');
                    
                    $stringFechaInicio = $anio."-".$mes."-01 00:00:00";
                    $stardDate = Carbon::createFromFormat('Y-m-d H:i:s', $stringFechaInicio, 'Europe/London');
                    $endDate = Carbon::now();
                    if(strcmp($tipo,$this->typeMensual)  === 0){
                        $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $stringFechaInicio, 'Europe/London')->endOfMonth();
                    }
                    $stringFechaFin = $endDate->format('Y-m-d H:i:s');
                    $params = [
                        "tipo" => $tipo,
                        "mes" => $mes,
                        "anio" => $anio,
                        "neg_id" => $idNegocio,
                        "fechaInicio" => $stringFechaInicio,
                        "fechaFin"=> $stringFechaFin
                    ];                    
                    $codigoResp = 500;
                    $mensaje = "No se generó el reporte";
                    $data= "0";
                    $rows = "0";
                    $generate= $logic->generarReporte($params, true);
                    
                    if(!is_null($generate["data"])){
                        $codigoResp = 200;
                        $mensaje= "OK";
                        $data = $generate["data"];
                        $rows = $generate["rows"];
                    }
                    
                    $return= response()-> json([
                        'message' => $mensaje,
                        'data' => $data,
                        'rows' => $rows
                    ],$codigoResp);                  
                }
            });
            return $return;
        }catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RepCliventaCab  $repCliventaCab
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $cabecera = RepCliventaCab::find($id);
        $detalle = null;
        if(!is_null($cabecera)){
            $detalle = RepCliventaDet::where('rcvd_rcvc_id', $id)
            ->join('tbl_cliente', 'tbl_rep_cliventadet.rcvd_cli_id', '=', 'tbl_cliente.cli_id')
            ->select('tbl_rep_cliventadet.*','cli_usuario_credito')->get();
            return response()-> json([
                'success' => true,
                'cabRepo' => $cabecera,
                'detRepo' => $detalle
                ],200); 
        }else{
            return response()-> json([
                'message' => 'Not Found'
                ],404);  
        }        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RepCliventaCab  $repCliventaCab
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {  
            $return ="INICIO";
            DB::transaction(function () use ($request, &$return) {
                $logic = new RepClienteVentaLogic();          
                $validator = Validator::make($request->all(), [
                    'idNegocio' => 'required',
                    'type' => 'required',
                    'year' => 'required',
                    'month' => 'required',
                    'startDate' => 'required',
                    'endDate' => 'required'
                    ]);
            
                if($validator->fails()){
                    $return= response()-> json([
                        'message' => 'Validation Error.',
                        'error' => $validator->errors()
                        ],500);               
                }else{                    
                    $idNegocio = $request->input('idNegocio');
                    $tipo = $request->input('type');
                    $anio = $request->input('year');    
                    $mes = $request->input('month');
                    $stringFechaInicio = $request->input('startDate');
                    $stringFechaFin = $request->input('endDate');

                    // $stardDate = Carbon::createFromFormat('Y-m-d H:i:s', $stringFechaInicio, 'Europe/London');
                    // $stardDate = Carbon::createFromFormat('Y-m-d H:i:s', $stringFechaFin, 'Europe/London');
                    
                    // $stringFechaInicio = $anio."-".$mes."-01 00:00:00";
                    // $stardDate = Carbon::createFromFormat('Y-m-d H:i:s', $stringFechaInicio, 'Europe/London');
                    // $endDate = Carbon::now();
                    // if(strcmp($tipo,$this->typeMensual)  === 0){
                    //     $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $stringFechaInicio, 'Europe/London')->endOfMonth();
                    // }
                    // $stringFechaFin = $endDate->format('Y-m-d H:i:s');
                    $params = [
                        "tipo" => $tipo,
                        "mes" => $mes,
                        "anio" => $anio,
                        "neg_id" => $idNegocio,
                        "fechaInicio" => $stringFechaInicio,
                        "fechaFin"=> $stringFechaFin
                    ];                    
                    $codigoResp = 500;
                    $mensaje = "No se generó el reporte";
                    $data= "0";
                    $rows = "0";
                    $generate= $logic->generarReporte($params, false);
                    
                    if(!is_null($generate["data"])){
                        $codigoResp = 200;
                        $mensaje= "OK";
                        $data = $generate["data"];
                        $rows = $generate["rows"];
                    }
                    
                    $return= response()-> json([
                        'message' => $mensaje,
                        'data' => $data,
                        'rows' => $rows
                    ],$codigoResp);                  
                }
            });
            return $return;
        }catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RepCliventaCab  $repCliventaCab
     * @return \Illuminate\Http\Response
     */
    public function destroy(RepCliventaCab $repCliventaCab)
    {
        //
    }
}
