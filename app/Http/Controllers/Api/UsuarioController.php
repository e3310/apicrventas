<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Usuario;
use App\Models\Persona;
use App\Models\Vendedor;
use App\Models\TipoUsuario;
use Illuminate\Http\Request;
use App\Http\Resources\UsuarioResource;
use Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{
    
    static function validateFieldsPersona($input){
        $validator = Validator::make($input, [
            'per_cedula' => 'required|max:10',
            'per_nombres' => 'required|min:2',
            'per_apellidos' => 'required|min:2',          
        ]);
        
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    static function validateFieldsPasswordNueva($input){

        $customMessages = [
            'different' => 'Contraseña nueva debe tener un valor diferente a Contraseña actual.',
            'regex' => 'Contraseña nueva no tiene un formato válido',
            'same' => 'Contraseña nueva debe tener un valor igual a Confirmación de contraseña'
        ];

        $validator = Validator::make($input, [
            'userId' => 'required',
            'userName' => 'required',
            'passOld' => 'required',
            'passNew' => ['required','min:8','max:16','different:passOld',
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[.,:;-_*]/', // must contain a special character
        ],
            'passNewConfirm' => 'required|same:passNew'
        ], $customMessages);
        
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    static function validateFieldsTipoNuevo($input){
        //nuevo tipo de usuario
        $validator = Validator::make($input, [
            'userId' => 'required',
            'tipoId' => 'required',
            'tipoNameNuevo' => 'required',
            'negocioId' => 'nullable'
        ]);
        
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    static function transformRequest(Request $request){
        $arrAux = [];
        foreach($request->all() as $key => $value){
            $newKey = "per_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }

    /**
     * Generate new User taking data from first name and last name
     *
     * @param  string  $nombres
     * @param  string  $apellidos
     * @return string $usuario (max:20)
     */
    function generarUsuario (string $nombres, string $apellidos, int $nroIntentos){ 
        $arrNombres = explode(" ", trim($nombres));
        $arrApellidos = explode(" ", trim($apellidos));
        $usuario = "";
        switch ($nroIntentos) {
            case 0: 
                $usuario = $arrNombres[0][0].$arrApellidos[0];
                break;
            case 1: 
                if(count($arrNombres)>1){
                    $usuario = $arrNombres[0][0].$arrNombres[1][0].$arrApellidos[0];
                }else 
                    if(count($arrApellidos)>1){ 
                    $usuario = $arrNombres[0][0].$arrApellidos[0].$arrApellidos[1][0];
                }else{
                // else if(strlen($arrNombres[0])>1){
                    $usuario = substr($arrNombres[0],0, 2).$arrApellidos[0];
                }
                break;
            default: 
                $nro = $nroIntentos >1? $nroIntentos-1: $nroIntentos;
                $usuario = $arrNombres[0][0].$nro.$arrApellidos[0];
                break;

        }
        return strtolower($usuario);
    }

    function verificarUsuario (Persona $persona){
        $esUnico = false;
        $nroIntentos = 0 ;
        $usuario = "";
        while ($esUnico == false) {
            $usuario = $this->generarUsuario($persona->per_nombres, $persona->per_apellidos, $nroIntentos);
            $buscarExistente = Usuario::where('us_usuario',$usuario)->count();
            if($buscarExistente >0){
                $nroIntentos++;
            }else{
                $esUnico = true;
            }
        }
        return $usuario;
    }

    function passwordAleatoria(){
        $mayusculas = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $minusculas = 'abcdefghijklmnopqrstuvwxyz';
        $numeros = '0123456789';
        $signos = '.,:;-_';
        $caracteres= $mayusculas.$minusculas.$numeros.$signos;
        $longpalabra=rand(8,16);
        for($pass='', $n=strlen($caracteres)-1; strlen($pass) < $longpalabra ; ) {
            $x = rand(0,$n);
            $pass.= $caracteres[$x];
        }
        // $longitud = 8; // longitud del password  
        // $pass = substr(md5(rand()),0,$longitud);  
        // return($pass); // devuelve el password  
        return $pass;
    }

    function storeUser(Persona $persona,string $tipo, $neg_id= null){
        $usuarioGenerado = $this->verificarUsuario($persona);
        $tipoId =TipoUsuario::where('tipo_nombre',$tipo)->take(1)->get();
        $pass = $this-> passwordAleatoria();
        $usuario = Usuario::create([
            'us_usuario' => $usuarioGenerado,
            'us_clave' => Crypt::encryptString($pass),
            'us_tipo_id' => $tipoId[0]->tipo_id,
            'us_per_id' => $persona->per_id,
            'us_neg_id' => $neg_id
        ]);
        return [
            'nameUser'=> $usuario->us_usuario,
            'keyUser' => $pass
        ];
    }

    function storeUserVendor(Persona $persona, Request $request){
        $vendedor = $usuario = Vendedor::create([
            'vend_per_id' => $persona->per_id,
            'vend_neg_id' => $request->neg_id,
            'vend_usuario_registro' => $request->usuario_registro
        ]);
        $usuario = null;
        if($vendedor->exists){            
            $usuario = $this->storeUser($persona, 'VENDEDOR',$request->neg_id);  
        }
        return $usuario;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAdmin()
    {
        return 
        UsuarioResource::collection(
            Usuario::join('tbl_tipousuario', 'tbl_usuario.us_tipo_id', '=', 'tbl_tipousuario.tipo_id')
        ->join('tbl_persona', 'us_per_id', 'tbl_persona.per_id')
        ->where('tbl_tipousuario.tipo_nombre','ADMINISTRADOR')
        ->select('tbl_persona.*', 'tipo_id','tipo_nombre','us_id', 'us_usuario', 'us_estado', 'us_neg_id')
        ->orderBy('per_apellidos','desc')->take(100)->get()
    );
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexVendedor(int $idNegocio)
    {
        return 
        UsuarioResource::collection(
            Usuario::join('tbl_tipousuario', 'tbl_usuario.us_tipo_id', '=', 'tbl_tipousuario.tipo_id')
        ->join('tbl_persona', 'us_per_id', 'tbl_persona.per_id')
        ->join('tbl_v_vendedor','per_id', 'tbl_v_vendedor.vend_per_id')
        ->where('tbl_usuario.us_neg_id',$idNegocio)
        ->select('tbl_persona.*', 'tipo_id','tipo_nombre','us_id', 'us_usuario', 'us_estado','vend_estado','vend_id','us_neg_id')
        ->orderBy('per_apellidos','desc')->take(100)->get()
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAdmin(Request $request)
    {
        try {  
            $return ="INICIO";
                DB::transaction(function () use ($request,&$return) {
                $input = $this->transformRequest($request);
                
                $validator = $this->validateFieldsPersona($input);
                if(!is_null($validator)){
                    $return = $validator;
                }else{
                    $personaOld = Persona::where('per_cedula',$request->cedula)->count();
                    if($personaOld <= 0){
                        $persona = Persona::create(array_map('strtoupper',$input));
                        if($persona->exists){
                            $usuario = $this->storeUser($persona,'ADMINISTRADOR');
                            $return = response()->json([
                                "success" => true,
                                "message" => "Usuario Administrador creado",
                                "data" => $usuario
                                ],200);
                        }else{
                            $return = response()->json([
                                "error" => "Error",
                                "message" => "Error al crear persona",
                                ],400);
                        }
                    }else{    
                        $return = response()->json([
                            "error" => "Error 400",
                            "message" => "Ya existe un usuario registrado con cedula {$request->cedula}",
                            ],400);            
                    }
                }
            });
            return $return;
        }catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
        // $nombres = "Eliana";
        // $apellidos ="Farinango Terán";
        // $usuarioGenerado = $this->generarUsuario($nombres, $apellidos,10);
        // $pass = $this->passwordAleatoria();
        // // $pass = Crypt::encryptString($request->cedula);
        // try {
        //     $decrypted = Crypt::decryptString('eyJpdiI6IkFCSkh4Q3phSDBPeGlveHIyRmE4UGc9PSIsInZhbHVlIjoiTXYxcldhU2ExemVHUFo2cFJTcWdVZz09IiwibWFjIjoiYjMyNGE1Y2MxODM3Mzg4MTlkZjQxMTAyMmFjYWM2NGQ3MjVmYjgwOGJjYjdlNzdjZDBjY2IwY2M5NjVmODFlNyJ9');
        // } catch (DecryptException $e) {
        //     //
        // }
        // $pass =$decrypted;
        // return response()->json([
        //     "success" => true,
        //     "message" => "Probando",
        //     // "data" => $request->except('cedula')
        //     "data" => $pass
        //     ],401);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeVendedor(Request $request)
    {
        // return $request;
        try {  
            $return ="INICIO";
                DB::transaction(function () use ($request,&$return) {
                $input = $this->transformRequest($request);              
                $validator = $this->validateFieldsPersona($input);
                if(!is_null($validator)){
                    $return = $validator;
                }else{
                    $personaOld = Persona::where('per_cedula',$request->cedula)->count();
                    if($personaOld <= 0){
                        $persona = Persona::create(array_map('strtoupper',$input));
                        if($persona->exists){
                            
                            $usuario = $this->storeUserVendor($persona, $request);
                            $return = response()->json([
                                "success" => true,
                                "message" => "Vendedor creado",
                                "data" => $usuario
                                ],200);
                        }else{
                            $return = response()->json([
                                "error" => "Error",
                                "message" => "Error al crear persona",
                                ],400);
                        }
                    }else{    
                        $return = response()->json([
                            "error" => "Error 400",
                            "message" => "Ya existe un usuario registrado con cedula {$request->cedula}",
                            ],400);            
                    }
                }
            });
            return $return;
        }catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    public function cambiarEstado(string $idUsuario)
    {
        try { 
            $usuario = Usuario::find($idUsuario);
            if(!is_null($usuario)){
                $estado = $usuario->us_estado;
                if($estado == 'A'){
                    $usuario->us_estado = 'I';
                }else{
                    $usuario->us_estado = 'A';
                }                
                $saved=$usuario->save();
                if($saved){
                    return response()->json([
                        "success" => true,
                        "message" => "Se cambio estado de Usuario",
                        // "data" => $usuario
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],$e->getStatusCode() );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request)
    {
        try { 
            // $input = $this->transformRequest($request);
            $validator = $this->validateFieldsPasswordNueva($request->all());
            if(!is_null($validator)){
                return $validator;
            }
            $updated = Usuario::where('us_id', $request->userId)
            ->where('us_usuario', $request->userName)
            ->where('us_clave',Crypt::encryptString($request->passOld))
            ->update(['us_clave' => Crypt::encryptString($request->passNew)]);
            if($updated){       
                return response()->json([
                    "success" => true,
                    "message" => "Contraseña editada",
                    "data" => $updated
                ],201);
            }else {
                return response()->json(['error' => 'No guardado'], 500);
            }
            
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function updateTipo(Request $request, string $idUsuario)
    {
        try {             
            $validator = $this->validateFieldsTipoNuevo($request->all());
            if(!is_null($validator)){
                return $validator;
            }
            $tipoNombre = $request->tipoNameNuevo;
            $tipoId =TipoUsuario::where('tipo_nombre',$tipoNombre)->take(1)->get();

            $usuario = Usuario::find($idUsuario);
            if(!is_null($usuario) && !is_null($tipoId)){
                $usuario->us_tipo_id = $tipoId[0]->tipo_id;
                $usuario->us_neg_id = $request->negocioId;
                $saved=$usuario->save();
                if($saved){
                    return response()->json([
                                "success" => true,
                                "message" => "Tipo editado"
                            ],201);
                } else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe información de usuario o tipo'], 404); 
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }
    

    public function activarPendiente(Request $request){
        try { 
            // $input = $this->transformRequest($request);
            $validator = $this->validateFieldsPasswordNueva($request->all());
            if(!is_null($validator)){
                return $validator;
            }
            $updated = Usuario::where('us_id', $request->userId)
            ->where('us_usuario', $request->userName)
            ->where('us_clave',Crypt::encryptString($request->passOld))
            ->where('us_estado','P')
            ->update(['us_clave' => Crypt::encryptString($request->passNew)],
            ['us_estado' => 'A']);
            if($updated){       
                return response()->json([
                    "success" => true,
                    "message" => "Usuario Activado",
                    "data" => $updated
                ],201);
            }else {
                return response()->json(['error' => 'No es usuario pendiente'], 503);
            }
            
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

}
