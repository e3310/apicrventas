<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Producto;
use App\Models\Para;
use Illuminate\Http\Request;
use App\Http\Resources\ProductoResource;
use Validator;

class ProductoController extends Controller
{
    static function validateFieldsProducto($input){
        $validator = Validator::make($input, [
            'pro_costo' => 'required',
            'pro_stock' => 'required',
            'pro_neg_id' => 'required'
        ]);
        
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    static function transformRequest(Request $request){
        $arrAux = [];
        foreach($request->all() as $key => $value){
            $newKey = "pro_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $idNegocio)
    {
        return ProductoResource::collection(Producto::where('pro_neg_id',$idNegocio)->take(100)->get());
    }

    /**
     * Display a count of items in list of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function count(int $idNegocio)
    {
        return Producto::select('pro_id')->where('pro_neg_id',$idNegocio)->count();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {            
            $parametroLogic = new ParametroGeneralLogic();
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsProducto($input);
            if(!is_null($validator)){
                return $validator;
            }else{                
                $buscarProducto = Producto::where('pro_nombre',"CREDITO")->count();
                if($buscarProducto === 0){
                    $return ="return";
                    DB::transaction(function () use ($idCompra, &$return) {
                        $producto = Producto::updateOrCreate(
                            ['pro_nombre' => 'CREDITO', 'pro_neg_id' => $input['pro_neg_id']],
                            ['pro_costo' => $input['pro_costo'], 'pro_precio' => $input['pro_precio'], 'pro_stock'=>$input['pro_stock'] ]
                        );
                        $parametroLogic->createParametro("productoCreado");
                        
                        $return = response()->json([
                            "success" => true,
                            "message" => "Producto guardado",
                            "data" => $producto
                            ],200);
                    });
                    return $return;
                }else{
                    return response()->json([
                        "success" => false,
                        "message" => "Ya se parametrizo"
                        ],405);
                }       
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }
}
