<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\KardexCab;
use App\Models\KardexDet;
use App\Models\KardexLogic;
use App\Models\VentaCab;
use App\Models\VentaDet;
use App\Models\Compra;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Resources\KardexResource;
use App\Http\Resources\VentaCabResource;
use App\Http\Resources\CompraResource;
use Validator;
use Illuminate\Support\Facades\DB;

class KardexController extends Controller
{
    private $fieldFechas = 'F';
    private $fieldMovimiento = 'M';

    private $movCompra ='C';
    private $movVenta ='V';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $idNegocio)
    {
        return KardexResource::collection(KardexCab::join('tbl_kardexdet', 'tbl_kardexcab.kar_id', '=', 'tbl_kardexdet.kd_kar_id')
        ->where('kar_neg_id',$idNegocio)
        ->where('kar_estado','A')
        ->orderBy('kd_id','desc')
        ->select('tbl_kardexcab.*', 'kd_cantidad', 'kd_total_detalle', 'kd_stock', 'kd_costo')->take(100)->get());
    }

    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KardexCab  $kardexCab
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idNegocio' => 'required',
            'idKardex' => 'required',
            'movimiento' => 'required'
        ]);
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else {
            $idNegocio = $request->input('idNegocio');
            $idKardex = $request->input('idKardex');
            $movimiento = $request->input('movimiento');
            switch($movimiento){
                case($this->movCompra):
                    return CompraResource::collection(Compra::where('com_neg_id',$idNegocio)
                        ->where('com_kar_id',$idKardex)
                        ->orderBy('com_fecha_registro','desc')->take(100)->get());
                    break;
                case($this->movVenta):
                    return VentaCabResource::collection(
                        VentaCab::join("tbl_v_vendedor","tbl_ventacab.ven_vend_id", '=', "tbl_v_vendedor.vend_id")
                        ->join("tbl_persona", "tbl_v_vendedor.vend_per_id", '=',"tbl_persona.per_id")
                        ->join("tbl_cliente", "tbl_ventacab.ven_cli_id", '=', "tbl_cliente.cli_id")
                        ->join("tbl_ventadet", 'tbl_ventacab.ven_id', '=', 'tbl_ventadet.vd_ven_id')
                        ->join("tbl_paquete", "tbl_ventadet.vd_paq_id", '=', "tbl_paquete.paq_id" )       
                        ->where('ven_neg_id', $idNegocio)
                        ->where('ven_kar_id', $idKardex)
                        ->orderBy('ven_fecha_registro','desc')
                        ->select('tbl_ventacab.*', 'vd_paq_id', 'vd_cantidad', 'vd_total_det', 'vd_bonificacion',
                        'paq_descripcion', 'per_nombres','per_apellidos', 'cli_usuario_credito')
                        ->take(100)->get());
                    break;
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'idNegocio' => 'required',
            'value1' => 'required',
            'value2' => 'required',
            'field'=> 'required'
        ]);

        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else {

            $idNegocio= $request->input('idNegocio');
            $parametroSearch1= strtoupper($request->input('value1'));
            $parametroSearch2= strtoupper($request->input('value2'));
            $fieldSearch = $request->input('field'); //F-> fechas, M -> movimientos

            return KardexResource::collection(
                KardexCab::join('tbl_kardexdet', 'tbl_kardexcab.kar_id', '=', 'tbl_kardexdet.kd_kar_id')
                ->where('kar_neg_id',$idNegocio)
                ->where('kar_estado','A')
                ->where(function($query) use ($parametroSearch1,$parametroSearch2, $fieldSearch) {
            switch($fieldSearch){
                case($this->fieldFechas):
                    $starDate = Carbon::createFromFormat('Y-m-d H:i:s', $parametroSearch1." "."00:00:00", 'Europe/London');
                    $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $parametroSearch2." "."23:59:59", 'Europe/London');
                    $stringInicio = $starDate->format('Y-m-d H:i:s');
                    $stringFin = $endDate->format('Y-m-d H:i:s');
                    $query->whereBetween('kar_fecha_registro', [$stringInicio, $stringFin]);
                    break;
                case($this->fieldMovimiento):
                    $query->where('kar_tipo_mov', $parametroSearch1);
                    break;
            }
        })->orderBy('kd_id','desc')
        ->select('tbl_kardexcab.*', 'kd_cantidad', 'kd_total_detalle', 'kd_stock', 'kd_costo')->take(100)->get()
        );
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\RepCliventaCab  $repCliventaCab
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request)
   {
    try {   
        $return ="INICIO";
            DB::transaction(function () use ($request, &$return) {
                $validator = Validator::make($request->all(), [
                    'idNegocio' => 'required',
                    'startDate' => 'required'
                ]);
        
                if($validator->fails()){
                    $return = response()-> json([
                        'message' => 'Validation Error.',
                        'error' => $validator->errors()
                        ],500);
                }else{
                    $kardexLogic = new KardexLogic();
                    $idNegocio = $request->input('idNegocio');
                    $stringFechaInicio = $request->input('startDate');
                    if($kardexLogic->verifyDateChangeStatus($stringFechaInicio) === true){                        
                        $idKardexPrev = $kardexLogic->idKardexPrev($stringFechaInicio);                                               
                        $recalculo = $kardexLogic->recalculoKardex($idKardexPrev->kar_id, $idNegocio);

                        $return = response()->json([
                            "success" => true,
                            "fecha" => $stringFechaInicio
                        ],201);
                    }else{
                        $return = response()->json([
                            "success" => false,
                            "message" => "Fecha no valida"
                        ],403);
                    }                 
                }
            });
            return $return;
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],$e->getStatusCode() );
        }
   }
}
