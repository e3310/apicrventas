<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Pais;
use Illuminate\Http\Request;
use App\Http\Resources\PaisResource;
use App\Http\Resources\PaisActivoResource;
use Validator;

class PaisController extends Controller
{
    public static function validateFieldsPais($input){
        // $input = $request->all();
        // $validator = Validator::make($input, [
        // 'cli_nombres' => 'required|max:100',
        // 'cli_apellidos' => 'required|max:100',
        // 'cli_telefono' => 'required|max:15',
        // 'cli_pais_id' => 'required',
        // 'cli_estado']);
        $validator = Validator::make($input, [
            'pais_nombre' => 'required|max:50'
        ]);
        
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    public static function transformRequest(Request $request){
        $arrAux = [];
        foreach($request->all() as $key => $value){
            $newKey = "pais_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PaisResource::collection(Pais::all());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getActivos(){
        return PaisActivoResource::collection(Pais::where('pais_estado','A')->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {            
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsPais($input);
            if(!is_null($validator)){
                return $validator;
            }
                        
            $pais = Pais::create($input);
            return response()->json([
            "success" => true,
            "message" => "Pais creado",
            "data" => $pais
            ],200);
        
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function show(Pais $pais)
    {
        return new  PaisResource($pais);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $idPais)
    {
        try { 
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsPais($input);
            if(!is_null($validator)){
                return $validator;
            }
            $paisOld = Pais::find($idPais);
            if(!is_null($paisOld)){
                $updated = $paisOld->update($input);  
                if($updated){       
                    return response()->json([
                        "success" => true,
                        "message" => "Pais editado",
                        "data" => $paisOld
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
            
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Update the state of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Negocio  $negocio
     * @return \Illuminate\Http\Response
     */
    public function cambiarEstado(int $idPais)
    {
        try { 
            $pais = Pais::find($idPais);
            if(!is_null($pais)){
                $estado = $pais->pais_estado;
                if($estado == 'A'){
                    $pais->pais_estado = 'I';
                }else{
                    $pais->pais_estado = 'A';
                }                
                $saved=$pais->save();
                if($saved){
                    return response()->json([
                        "success" => true,
                        "data" => $pais
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],$e->getStatusCode() );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pais $pais)
    {
        //
    }
    
}
