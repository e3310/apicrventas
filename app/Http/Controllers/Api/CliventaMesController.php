<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CliVentasMes;
use Illuminate\Http\Request;

class CliventaMesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CliVentasMes  $cliVentasMes
     * @return \Illuminate\Http\Response
     */
    public function show(CliVentasMes $cliVentasMes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CliVentasMes  $cliVentasMes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CliVentasMes $cliVentasMes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CliVentasMes  $cliVentasMes
     * @return \Illuminate\Http\Response
     */
    public function destroy(CliVentasMes $cliVentasMes)
    {
        //
    }
}
