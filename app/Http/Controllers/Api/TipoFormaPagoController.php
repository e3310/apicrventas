<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TipoFormaPago;
use Illuminate\Http\Request;
use App\Http\Resources\TipoFormaPagoResource;
use App\Http\Resources\TipoFormaPagoActivaResource;
use Validator;

class TipoFormaPagoController extends Controller
{
    public static function validateFieldsFormaPago($input){
        $validator = Validator::make($input, [
        'tfp_nombre' => 'required|max:45',
        'tfp_descripcion' => 'nullable|max:150',
        'tfp_neg_id' => 'required'
        ]);
       
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    public static function transformRequest(Request $request){
        $arrAux = [];
        foreach($request->all() as $key => $value){
            $newKey = "tfp_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $idNegocio)
    {
        return TipoFormaPagoResource::collection(TipoFormaPago::where('tfp_neg_id',$idNegocio)->take(100)->get());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getActivos(int $idNegocio){
        return TipoFormaPagoActivaResource::collection(TipoFormaPago::where('tfp_estado','A')
        ->where('tfp_neg_id',$idNegocio)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {            
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsFormaPago($input);
            if(!is_null($validator)){
                return $validator;
            }
            $formaPagoOld = TipoFormaPago::where('tfp_nombre',$request->nombre)->count();
            if($formaPagoOld <= 0){
                $formaPago = TipoFormaPago::create(array_map('strtoupper',$input));
                return response()->json([
                "success" => true,
                "message" => "TipoFormaPago creada",
                // "data" => $formaPago
                ],200);
            }else{    
                // throw new Exception(400, "Ya existe un formaPago registrado con el usuario");
                return response()->json([
                    "error" => "Error 400",
                    "message" => "Ya existe una formaPago registrada con el nombre {$request->nombre}",
                    // "data" => $formaPagoOld
                    ],400);  
                // }                 
            }        
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TipoFormaPago  $formaPago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $idFormaPago)
    {
        try { 
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsFormaPago($input);
            if(!is_null($validator)){
                return $validator;
            }
            $formaPagoOld = TipoFormaPago::find($idFormaPago);
            if(!is_null($formaPagoOld)){
                $updated = $formaPagoOld->update(array_map('strtoupper',$input));  
                if($updated){       
                    return response()->json([
                        "success" => true,
                        "message" => "TipoFormaPago editada",
                        "data" => $formaPagoOld
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
            
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Update the state of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TipoFormaPago  $formaPago
     * @return \Illuminate\Http\Response
     */
    public function cambiarEstado(string $idFormaPago)
    {
        try { 
            $formaPago = TipoFormaPago::find($idFormaPago);
            if(!is_null($formaPago)){
                $estado = $formaPago->tfp_estado;
                if($estado == 'A'){
                    $formaPago->tfp_estado = 'I';
                }else{
                    $formaPago->tfp_estado = 'A';
                }                
                $saved=$formaPago->save();
                if($saved){
                    return response()->json([
                        "success" => true,
                        // "message" => "Se cambio estado de TipoFormaPago",
                        "data" => $formaPago
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],$e->getStatusCode() );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TipoFormaPago  $formaPago
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoFormaPago $formaPago)
    {
        //
    }
}
