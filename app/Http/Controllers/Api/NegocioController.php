<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Negocio;
use Illuminate\Http\Request;
use App\Http\Resources\NegocioResource;
use Validator;
use Illuminate\Support\Facades\Log;

class NegocioController extends Controller
{

    public static function validateFieldsNegocio($input){
        // $input = $request->all();
        $validator = Validator::make($input, [
        'neg_nombre' => 'required|max:100',
        'neg_logo' => 'nullable']);
        
        if($validator->fails()){
            return response()-> json([
                'message' => 'Validation Error.',
                'error' => $validator->errors()
                ],500);               
        }else return null;
    }

    public static function transformRequest(Request $request){
        $arrAux = [];
        foreach($request->all() as $key => $value){
            $newKey = "neg_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return NegocioResource::collection(Negocio::latest('neg_fecha_registro')->paginate());
        return NegocioResource::collection(Negocio::all());
//         $negocios = Negocio::all();
// return response()->json([
// "success" => true,
// "message" => "Product List",
// "data" => $products
// ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {            
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsNegocio($input);
            if(!is_null($validator)){
                return $validator;
            }
                        
            $negocio = Negocio::create($input);
            return response()->json([
            "success" => true,
            "message" => "Negocio creado",
            "data" => $negocio
            ],200);
        
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Negocio  $negocio
     * @return \Illuminate\Http\Response
     */
    public function show(Negocio $negocio)
    {
        return new  NegocioResource($negocio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Negocio  $idNegocio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $idNegocio)
    {
        try { 
            $input = $this->transformRequest($request);
            $validator = $this->validateFieldsNegocio($input);
            if(!is_null($validator)){
                return $validator;
            }
            $negocioOld = Negocio::find($idNegocio);
            if(!is_null($negocioOld)){
                $updated = $negocioOld->update($input);  
                if($updated){       
                    return response()->json([
                        "success" => true,
                        "message" => "Negocio editado",
                        "data" => $negocioOld
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
            
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], $e->getStatusCode());
        }
    }

    /**
     * Update the state of specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Negocio  $negocio
     * @return \Illuminate\Http\Response
     */
    public function cambiarEstado(int $idNegocio)
    {
        try { 
            $negocio = Negocio::find($idNegocio);
            if(!is_null($negocio)){
                $estado = $negocio->neg_estado;
                if($estado == 'A'){
                    $negocio->neg_estado = 'I';
                }else{
                    $negocio->neg_estado = 'A';
                }                
                $saved=$negocio->save();
                if($saved){
                    return response()->json([
                        "success" => true,
                        // "message" => "Se cambio estado de Negocio",
                        "data" => $negocio
                    ],201);
                }else {
                    return response()->json(['error' => 'No guardado'], 500);
                }
            }else{
                return response()->json(['error' => 'No existe'], 404); 
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()],$e->getStatusCode() );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Negocio  $negocio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Negocio $negocio)
    {
        // $negocio ->delete();
        // return response()-> json([
        //     'message' => 'Success'
        // ],204);
    }
}
