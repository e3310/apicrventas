<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UsuarioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "idPersona" => $this->per_id,
            "cedula"=> $this->per_cedula,
            "nombres"=> $this->per_nombres,
            "apellidos"=> $this->per_apellidos,
            "genero"=> $this->per_genero,
            "telefono"=> $this->per_telefono,
            "email"=> $this->per_email,
            "direccion"=> $this->per_direccion,
            "ciudad"=> $this->per_ciudad,
            "pais"=> $this->per_pais,
            "tipo_nombre"=> $this->tipo_nombre,
            "idUsuario"=> $this->us_id,
            "nameUsuario"=> $this->us_usuario,
            "estado"=> $this->us_estado,
            "estadoVend" => $this->vend_estado,
            "idVendedor" => $this->vend_id,
            "neg_id"=> $this->us_neg_id,
            "idTipo" => $this->tipo_id
        ];
    }
}
