<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaqueteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->paq_id,
            'descripcion' => $this->paq_descripcion,
            'prod_id' => $this->paq_prod_id,
            'nro_unidades' => $this->paq_nro_unidades,
            'precio_venta' => $this->paq_precio_venta,
            'estado'=> $this->paq_estado,
            'neg_id' => $this->paq_neg_id
        ];
    }
}
