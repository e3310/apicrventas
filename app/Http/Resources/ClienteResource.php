<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClienteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->cli_id,
            'usuario_credito' => $this->cli_usuario_credito,
            'nombres' => $this->cli_nombres,
            'apellidos' => $this->cli_apellidos,
            'telefono' => $this->cli_telefono,
            'pais_id' => $this ->cli_pais_id,
            'ci' => $this->cli_cli,
            'ciudad' => $this->cli_ciudad,
            'direccion' => $this->cli_direccion,
            'email' => $this->cli_email,
            'genero' => $this->cli_genero,
            'estado' => $this->cli_estado,
            'neg_id' => $this->cli_neg_id,
            'usuario_registro'=> $this->cli_usuario_registro,
            'fecha_registro' => $this->cli_fecha_registro,
            'pais_nombre' => $this->pais_nombre             
        ];
    }
}
