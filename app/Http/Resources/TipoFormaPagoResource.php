<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TipoFormaPagoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->tfp_id,
            'nombre' => $this->tfp_nombre,
            'descripcion' => $this->tfp_descripcion,
            'estado' => $this->tfp_estado,     
            'neg_id' => $this->tfp_neg_id
        ];
    }
}
