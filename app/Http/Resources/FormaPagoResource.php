<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FormaPagoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->fp_id,
            'numero' => $this->fp_numero,
            'tipo_cuenta' => $this->fp_tipo_cuenta,
            'tipofp' => $this->fp_tipofp,
            'saldo' => $this->fp_saldo,
            'entidad_bancaria' => $this->fp_entidad_bancaria,
            'nombres_titular' => $this->fp_nombres_titular,            
            'apellidos_titular' => $this->fp_apellidos_titular,
            'ci_titular' => $this->fp_ci_titular,
            'telefono_titular' => $this->fp_telefono_titular,
            'email_titular' => $this->fp_email_titular,
            'estado' => $this->fp_estado,
            'neg_id' => $this->fp_neg_id,
            'usuario_registro' => $this->fp_usuario_registro,
            'fecha_registro' => $this->fp_fecha_registro
        ];
    }
}
