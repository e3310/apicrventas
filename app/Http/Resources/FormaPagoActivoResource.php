<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FormaPagoActivoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->fp_id,
            'numero' => $this->fp_numero,
            'nombres_titular' => $this->fp_nombres_titular,            
            'apellidos_titular' => $this->fp_apellidos_titular
        ];
    }
}
