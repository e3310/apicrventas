<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VendedorActivoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "nombres"=> $this->per_nombres,
            "apellidos"=> $this->per_apellidos,
            "idVendedor" => $this->vend_id
        ];
    }
}
