<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VentasDetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->vd_id,
            "vend_id"=> $this->vd_vend_id,
            "paq_id"=> $this->vd_paq_id,
            "cantidad"=> $this->vd_cantidad,
            "total_det"=> $this->vd_total_det
        ];
    }
}
