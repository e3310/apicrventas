<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->pro_id,
            'nombre' => $this->pro_nombre,
            'costo' => $this->pro_costo,
            'precio' => $this->pro_precio,
            'stock' => $this->pro_stock,
            'neg_id' => $this->pro_neg_id
        ];
    }
}
