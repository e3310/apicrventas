<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransferenciaBancariaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->trb_id,
            "fp_id"=> $this->trb_fp_id,
            "total_transferencia"=> $this->trb_total_transferencia,
            "costo_transferencia"=> $this->trb_costo_transferencia,
            "total_transferido"=> $this->trb_total_transferido,
            "neg_id"=> $this->trb_neg_id,
            "fecha_transferencia"=> $this->trb_fecha_transferencia,
            "fecha_registro"=> $this->trb_fecha_registro,
            "usuario_registro"=> $this->trb_usuario_registro,
            "estado"=> $this->trb_estado
        ];
    }
}
