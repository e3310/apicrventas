<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NegocioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->neg_id,
            'nombre' => $this->neg_nombre,
            'estado' => $this-> neg_estado,
            'usuarioRegistro' => $this-> neg_usuario_registro,
            'fechaRegistro' => $this -> neg_fecha_registro,
            'logo' => $this -> logo
            // "neg_nombre": "negocio",
            // "neg_estado": "A",
            // "neg_usuario_registro": null,
            // "neg_fecha_registro": "2021-06-16 18:52:30",
            // "neg_logo": null
        ];
    }
}
