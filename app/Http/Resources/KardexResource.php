<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class KardexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->kar_id,
            'fechaRegistro' => $this->kar_fecha_registro,
            'movimiento' => $this->kar_tipo_mov,
            'neg_id' => $this->kar_neg_id,
            'estado' => $this->kar_estado,
            'cantidad' => $this->kd_cantidad,
            'total_detalle' => $this->kd_total_detalle,
            'stock' => $this->kd_stock,
            'costo' => $this ->kd_costo
        ];
    }
}
