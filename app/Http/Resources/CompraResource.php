<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompraResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->com_id,
            'pro_id' => $this->com_pro_id,
            'cantidad' => $this->com_cantidad,
            'costo' => $this->com_costo,
            'total' => $this->com_total,
            'estado' => $this->com_estado,
            'fecha_registro' => $this->com_fecha_registro,            
            'neg_id' => $this->com_neg_id,
            'usuario_registro' => $this->com_usuario_registro
        ];
    }
}
