<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'userKey' => $this->us_id,
            'userName' => $this->us_usuario,
            'typeKey' => $this->us_tipo_id,
            'typeName' => $this->tipo_nombre,
            'page' => $this->spag_nombre,
            'abr' => $this->spag_abr,
            'url' => $this->spag_ruta,
            'icon' => $this->spag_icono_fs,
            'accion' => $this->sam_nombre,
            'idNegocio' => $this->us_neg_id
            // "neg_nombre": "negocio",
            // "neg_estado": "A",
            // "neg_usuario_registro": null,
            // "neg_fecha_registro": "2021-06-16 18:52:30",
            // "neg_logo": null
        ];
    }
}
