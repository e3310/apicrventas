<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VentaCabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->ven_id,
            "cli_id"=> $this->ven_cli_id,
            "vend_id"=> $this->ven_vend_id,
            "tfp_id"=> $this->ven_tfp_id,
            "fp_id"=> $this->ven_fp_id,
            "fecha_registro"=> $this->ven_fecha_registro,
            "kar_id"=> $this->ven_kar_id,
            // "total"=> $this->ven_total,
            "estado" => $this->ven_estado,
            "neg_id"=> $this->ven_neg_id,
            "usuario_registro"=> $this->ven_usuario_registro,
            //detalle
            "paq_id" => $this->vd_paq_id,
            "cantidad" => $this->vd_cantidad,
            "total_det" => $this->vd_total_det,
            "bonificacion" => $this->vd_bonificacion,
            //detalles adicionales
            "nombrePaquete" => $this->paq_descripcion,
            "nombreVendedor" => $this->per_nombres. " " .$this->per_apellidos,
            "userCliente" => $this->cli_usuario_credito
        ];
    }
}
