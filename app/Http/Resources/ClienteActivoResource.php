<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClienteActivoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->cli_id,
            'usuario_credito' => $this->cli_usuario_credito,
            'nombres' => $this->cli_nombres,
            'apellidos' => $this->cli_apellidos          
        ];
    }
}
