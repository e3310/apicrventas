<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RepCliVentaCabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->rcvc_id,
            "tipo"=> $this->rcvc_tipo,
            "mes"=> $this->rcvc_mes,
            "anio"=> $this->rcvc_anio,
            "neg_id"=> $this->rcvc_neg_id,
            "fecha_inicio"=> $this->rcvc_fecha_inicio,
            "fecha_fin"=> $this->rcvc_fecha_fin,
            "fecha_generado"=> $this->rcvc_fecha_generado
        ];
    }
}
