<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $spag_id
 * @property int $spag_modulo
 * @property string $spag_nombre
 * @property string $spag_abr
 * @property string $spag_ruta
 * @property string $spag_icono_fs
 * @property int $spag_is_main
 * @property TblSegModulo $tblSegModulo
 * @property TblSegRelAccionPagina[] $tblSegRelAccionPaginas
 */
class SeguridadPagina extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_seg_pagina';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'spag_id';

    /**
     * @var array
     */
    protected $fillable = ['spag_modulo', 'spag_nombre', 'spag_abr', 'spag_ruta', 'spag_icono_fs', 'spag_is_main'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblSegModulo()
    {
        return $this->belongsTo('App\TblSegModulo', 'spag_modulo', 'smod_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblSegRelAccionPaginas()
    {
        return $this->hasMany('App\TblSegRelAccionPagina', 'srap_pag_id', 'spag_id');
    }
}
