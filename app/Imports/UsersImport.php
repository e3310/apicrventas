<?php

namespace App\Imports;

use App\Models\Pais;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel, WithBatchInserts, WithUpserts
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        return new Pais([
        //    'pais_neg_id'     => $row[0],
           'pais_nombre'    => $row[0]
        ]);
    }
}