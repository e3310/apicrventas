<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $srap_id
 * @property int $srap_pag_id
 * @property int $srap_accion_id
 * @property string $srap_estado
 * @property TblSegAccionmodelo $tblSegAccionmodelo
 * @property TblSegPagina $tblSegPagina
 * @property TblSegRelPermisopagina[] $tblSegRelPermisopaginas
 */
class SeguridadAccionPorPagina extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_seg_rel_accion_pagina';

    /**
     * @var array
     */
    protected $fillable = ['srap_estado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblSegAccionmodelo()
    {
        return $this->belongsTo('App\TblSegAccionmodelo', 'srap_accion_id', 'sam_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblSegPagina()
    {
        return $this->belongsTo('App\TblSegPagina', 'srap_pag_id', 'spag_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblSegRelPermisopaginas()
    {
        return $this->hasMany('App\TblSegRelPermisopagina', 'srpp_accion_pag_id', 'srap_id');
    }
}
