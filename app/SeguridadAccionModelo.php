<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $sam_id
 * @property string $sam_nombre
 * @property string $sam_descripcion
 * @property TblSegRelAccionPagina[] $tblSegRelAccionPaginas
 */
class SeguridadAccionModelo extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_seg_accionmodelo';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'sam_id';

    /**
     * @var array
     */
    protected $fillable = ['sam_nombre', 'sam_descripcion'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblSegRelAccionPaginas()
    {
        return $this->hasMany('App\TblSegRelAccionPagina', 'srap_accion_id', 'sam_id');
    }
}
