<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $smod_id
 * @property string $smod_nombre
 * @property string $smod_icono
 * @property string $smod_estado
 * @property TblSegPagina[] $tblSegPaginas
 */
class SeguridadModulo extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_seg_modulo';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'smod_id';

    /**
     * @var array
     */
    protected $fillable = ['smod_nombre', 'smod_icono', 'smod_estado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblSegPaginas()
    {
        return $this->hasMany('App\TblSegPagina', 'spag_modulo', 'smod_id');
    }
}
