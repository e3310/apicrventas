<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $srpp_id
 * @property int $srpp_tipo_us
 * @property int $srpp_accion_pag_id
 * @property string $srrp_estado
 * @property TblSegRelAccionPagina $tblSegRelAccionPagina
 * @property TblTipousuario $tblTipousuario
 */
class SeguridadPermisoPorPagina extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_seg_rel_permisopagina';

    /**
     * @var array
     */
    protected $fillable = ['srrp_estado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblSegRelAccionPagina()
    {
        return $this->belongsTo('App\TblSegRelAccionPagina', 'srpp_accion_pag_id', 'srap_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblTipousuario()
    {
        return $this->belongsTo('App\TblTipousuario', 'srpp_tipo_us', 'tipo_id');
    }
}
