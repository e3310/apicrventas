<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $vtam_id
 * @property int $vtam_cli_id
 * @property string $vtam_nro_creditos
 * @property string $vtam_mes
 * @property string $vtam_anio
 * @property string $vtam_fecha_cierre
 * @property string $vtam_fecha_registro
 * @property TblCliente $tblCliente
 */
class CliVentaMes extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_cliventames';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'vtam_id';

    /**
     * @var array
     */
    protected $fillable = ['vtam_cli_id', 'vtam_nro_creditos', 'vtam_mes', 'vtam_anio', 'vtam_fecha_cierre', 'vtam_fecha_registro'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblCliente()
    {
        return $this->belongsTo('App\TblCliente', 'vtam_cli_id', 'cli_id');
    }
}
