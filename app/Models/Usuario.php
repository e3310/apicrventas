<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int $us_id
 * @property int $us_tipo_id
 * @property int $us_per_id
 * @property int $us_neg_id
 * @property string $us_usuario
 * @property string $us_clave
 * @property string $us_estado
 * @property TblNegocio $tblNegocio
 * @property TblTipousuario $tblTipousuario
 * @property TblPersona $tblPersona
 */
class Usuario extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_usuario';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'us_id';

    /**
     * @var array
     */
    protected $fillable = ['us_tipo_id','us_per_id', 'us_neg_id', 'us_usuario', 'us_clave', 'us_estado'];

    use HasApiTokens;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'us_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblTipousuario()
    {
        return $this->belongsTo('App\TblTipousuario', 'us_tipo_id', 'tipo_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblPersona()
    {
        return $this->belongsTo('App\TblPersona', 'us_per_id', 'per_id');
    }
}
