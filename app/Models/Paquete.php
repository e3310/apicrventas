<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $paq_id
 * @property int $paq_prod_id
 * @property int $paq_neg_id
 * @property string $paq_descripcion
 * @property int $paq_nro_unidades
 * @property float $paq_precio_venta
 * @property string $paq_estado
 * @property TblNegocio $tblNegocio
 * @property TblProducto $tblProducto
 * @property TblVentadet[] $tblVentadets
 */
class Paquete extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_paquete';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'paq_id';

    /**
     * @var array
     */
    protected $fillable = ['paq_prod_id', 'paq_neg_id', 'paq_descripcion', 'paq_nro_unidades', 'paq_precio_venta', 'paq_estado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'paq_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblProducto()
    {
        return $this->belongsTo('App\TblProducto', 'paq_prod_id', 'pro_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblVentadets()
    {
        return $this->hasMany('App\TblVentadet', 'vd_paq_id', 'paq_id');
    }
}
