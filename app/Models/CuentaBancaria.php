<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $ctab_id
 * @property int $ctab_neg_id
 * @property string $ctab_numero
 * @property string $ctab_tipo_cuenta
 * @property float $ctab_saldo
 * @property string $ctab_entidad_bancaria
 * @property string $ctab_nombres_titular
 * @property string $ctab_apellidos_titular
 * @property string $ctab_ci_titular
 * @property string $ctab_telefono_titular
 * @property string $ctab_email_titular
 * @property string $ctab_estado
 * @property int $ctab_usuario_registro
 * @property string $ctab_fecha_registro
 * @property TblNegocio $tblNegocio
 * @property TblTransferenciabancarium[] $tblTransferenciabancarias
 * @property TblVentacab[] $tblVentacabs
 */
class CuentaBancaria extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_cuentabancaria';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'ctab_id';

    /**
     * @var array
     */
    protected $fillable = ['ctab_neg_id', 'ctab_numero', 'ctab_tipo_cuenta', 'ctab_saldo', 'ctab_entidad_bancaria', 'ctab_nombres_titular', 'ctab_apellidos_titular', 'ctab_ci_titular', 'ctab_telefono_titular', 'ctab_email_titular','ctab_estado', 'ctab_usuario_registro', 'ctab_fecha_registro'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'ctab_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblTransferenciabancarias()
    {
        return $this->hasMany('App\TblTransferenciabancarium', 'trb_ctab_id', 'ctab_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblVentacabs()
    {
        return $this->hasMany('App\TblVentacab', 'ven_ctab_id', 'ctab_id');
    }
}
