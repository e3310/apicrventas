<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $rcvd_id
 * @property int $rcvd_rcvc_id
 * @property int $rcvd_cli_id
 * @property int $rcvd_nro_creditos 
 * @property int $rcvd_bonificacion
 * @property TblCliente $tblCliente
 * @property RepCliVentaCb[] $repCliVentaCb
 */
class RepCliVentaDet extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_rep_cliventadet';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'rcvd_id';

    /**
     * @var array
     */
    protected $fillable = ['rcvd_rcvc_id', 'rcvd_cli_id', 'rcvd_nro_creditos','rcvd_bonificacion'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblCliente()
    {
        return $this->belongsTo('App\TblCliente', 'rcvd_cli_id', 'cli_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function repCliVentaCb()
    {
        return $this->belongsTo(\App\Models\RepCliVentaCb::class, 'rcvc_id', 'rcvd_rcvc_id');
    }
}
