<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\RepCliventaCab;
use App\Models\RepCliventaDet;
use App\Models\Cliente;
use App\Models\VentaCab;
use App\Models\VentaDet;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Symfony\Component\HttpKernel\Exception\HttpException;

class RepClienteVentaLogic extends Model
{
    // use HasFactory;

    public function generarReporte($paramsCab, $isFirstCreate = true){
        $cabecera = $this->FindCabReporte($paramsCab);
        $reporte =0;
        $result = null;
        if(is_null($cabecera)){
            if($isFirstCreate === true){
                $result = $this->createNuevoReporte($paramsCab);                
            }else{
                $reporte = 4444;
            }
        }else{
            if($isFirstCreate === false)
            {
                $result = $this->actualizarReporte($paramsCab, $cabecera);
            }else{
                $reporte = 4444;
            }
        }
        return $result;  
    }

    function createNuevoReporte($paramsCab){
        $ventasMass = $this->getInfoReporte($paramsCab["fechaInicio"], $paramsCab["fechaFin"]);
        if(count($ventasMass->get()->toArray()) >0){
            $cabecera = $this->CreateCabReporte($paramsCab);
            $detalles = $this->generarListaDetallesReporte($ventasMass->get(), $cabecera, "rcvd_rcvc_id");
            $cabecera->repCliVentaDets()->saveMany($detalles);
            return ["data" => $cabecera, 'rows'=>count($ventasMass->get()->toArray())];  
        }else{
            return ["data" => null, 'rows'=> count($ventasMass->get()->toArray())];
        }
        
    }

    function createNuevoReporteFromArray($ventas){
        $ventasCollection = collect($ventas);
        $uniqueVerify = array();
        $uniqueCabs = array();
        foreach($ventas as $v)
        {
            $verify = 'A'.$v['rcvd_anio'].'M'.$v['rcvd_mes'].'N'.$v['rcvd_neg_id'];
            if(!in_array($verify, $uniqueVerify)){
                $stringFechaInicio = $v['rcvd_anio']."-".$v['rcvd_mes']."-01 00:00:00";
                $stardDate = Carbon::createFromFormat('Y-m-d H:i:s', $stringFechaInicio, 'Europe/London');
                $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $stringFechaInicio, 'Europe/London')->endOfMonth();
                $stringFechaFin = $endDate->format('Y-m-d H:i:s');
                $paramsCab = [
                    "tipo" => $v["rcvd_tipo"],
                    "mes" => $v["rcvd_mes"],
                    "anio" => $v["rcvd_anio"],
                    "mes" => $v["rcvd_mes"],
                    "neg_id" => $v["rcvd_neg_id"],
                    "fechaInicio" => $stringFechaInicio,
                    "fechaFin" => $stringFechaFin
                ];
                if(is_null($this->FindCabReporte($paramsCab))){
                    array_push($uniqueVerify, $verify);
                    $cabecera = $this->CreateCabReporte($paramsCab);
                    $buscarDets = $ventasCollection->where("rcvd_tipo",$v["rcvd_tipo"])
                    ->where("rcvd_mes",$v["rcvd_mes"])->where("rcvd_anio",$v["rcvd_anio"])
                    ->where("rcvd_neg_id",$v["rcvd_neg_id"])->all();
                    $detalles = $this->generarListaDetallesReporte($buscarDets, $cabecera, "rcvd_rcvc_id");
                    $cabecera->repCliVentaDets()->saveMany($detalles); 
                    // $detalles = $this->generarListaDetFromArray($buscarDets, $cabecera);
                    // $cabecera +=['repCliVentaDets' => $detalles];
                    // array_push($uniqueCabs, $cabecera);
                }

            //     $cabecera = ["tipo"=>"M",
            //         // $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $stringFechaInicio, 'Europe/London')->endOfMonth();
            //     ];
            //     // $v+=['']
            //     // array_push($uniqueCabs, $v);
            //     // array_push($uniqueVerify, $verify);

            }
        }
        return $uniqueCabs;
    }

    function actualizarReporte($paramsCab, $cabecera){
        $cabecera->rcvc_fecha_generado = Carbon::now()->format('Y-m-d H:i:s');
        $savedCab = $cabecera->save();        
        $ventasMass = $this->getInfoReporte($paramsCab["fechaInicio"], $paramsCab["fechaFin"]);
        if(count($ventasMass->get()->toArray()) >0 && $savedCab){
            $cabecera->repCliVentaDets()->delete();
            $detalles = $this->generarListaDetallesReporte($ventasMass->get(), $cabecera, "rcvd_rcvc_id");
            $cabecera->repCliVentaDets()->saveMany($detalles);            
        }
        return ["data" => $cabecera, 'rows'=> count($ventasMass->get()->toArray()) >0];  
    }

    function generarListaDetallesReporte($arrayDetalles, RepCliventaCab $cabecera, string $campoFK){
        $detallesAux= [];
        foreach ($arrayDetalles as $detalle) {
            $d = new RepCliVentaDet([
                    "rcvd_rcvc_id" => $cabecera->rcvc_id,
                    "rcvd_cli_id" => $detalle["rcvd_cli_id"],
                    "rcvd_nro_creditos"=> $detalle["rcvd_nro_creditos"],
                    "rcvd_bonificacion" =>$detalle["rcvd_bonificacion"]
            ]);
            array_push($detallesAux, $d);
        }
        return $detallesAux;
    }

    function generarListaDetFromArray($arrayDetalles, $cabecera){
        $detallesAux= [];
        foreach ($arrayDetalles as $detalle) {
            $d = [
                    // "rcvd_rcvc_id" => $cabecera->rcvc_id,
                    "rcvd_cli_id" => $detalle["rcvd_cli_id"],
                    "rcvd_nro_creditos"=> $detalle["rcvd_nro_creditos"],
                    "rcvd_bonificacion" =>$detalle["rcvd_bonificacion"]
            ];
            array_push($detallesAux, $d);
        }
        return $detallesAux;
    }

    function CreateCabReporte($paramsCab){
        return RepCliventaCab::create([
            'rcvc_tipo' => $paramsCab["tipo"],
            'rcvc_mes' => $paramsCab["mes"],
            'rcvc_anio' => $paramsCab["anio"],
            'rcvc_neg_id'=> $paramsCab["neg_id"],
            'rcvc_fecha_inicio' => $paramsCab["fechaInicio"],
            'rcvc_fecha_fin' => $paramsCab["fechaFin"]
        ]);
    }

    function AsignarCabReporteArray($paramsCab){
        return [
            'rcvc_tipo' => $paramsCab["tipo"],
            'rcvc_mes' => $paramsCab["mes"],
            'rcvc_anio' => $paramsCab["anio"],
            'rcvc_neg_id'=> $paramsCab["neg_id"],
            'rcvc_fecha_inicio' => $paramsCab["fechaInicio"],
            'rcvc_fecha_fin' => $paramsCab["fechaFin"]
        ];
    }

    

    function FindCabReporte($paramsCab){
        return RepCliventaCab::where('rcvc_tipo',$paramsCab["tipo"])
        ->where('rcvc_mes', $paramsCab["mes"])
        ->where('rcvc_anio', $paramsCab["anio"])
        ->where('rcvc_neg_id', $paramsCab["neg_id"])
        ->where('rcvc_fecha_inicio', $paramsCab["fechaInicio"])
        // ->where('rcvc_fecha_fin', $paramsCab["fechaFin"])
        ->first();
    }

    function getInfoReporte(string $fechaInicio, string $fechaFin){
        return VentaCab::whereBetween('ven_fecha_registro',[$fechaInicio, $fechaFin])
        ->join('tbl_ventadet','tbl_ventacab.ven_id','=','tbl_ventadet.vd_ven_id')
        ->join('tbl_paquete', 'tbl_ventadet.vd_paq_id','=','tbl_paquete.paq_id')
        ->select('tbl_ventacab.ven_cli_id as rcvd_cli_id',
        DB::raw("(sum(vd_cantidad * paq_nro_unidades)) as rcvd_nro_creditos"),
        DB::raw("(sum(vd_bonificacion)) as rcvd_bonificacion"),
        )->groupBy('ven_cli_id');
    }

    public static function transformRequestDetalles($detalles){
        $arrAux = [];
        foreach($detalles as $key => $value){
            $newKey = "rcvd_".preg_replace("/(_?\d+)+$/","",$key); //this generates the name of column that you need
            $arrAux[$newKey] = $value;
        }
        return $arrAux;
    }
}
