<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $ven_id
 * @property int $ven_cli_id
 * @property int $ven_vend_id
 * @property int $ven_tfp_id
 * @property int $ven_fp_id
 * @property int $ven_kar_id
 * @property int $ven_neg_id
 * @property string $ven_fecha_registro
 * @property string $ven_estado
 * @property float $ven_total
 * @property int $ven_usuario_registro
 * @property TblCliente $tblCliente
 * @property TblNegocio $tblNegocio
 * @property TblVVendedor $tblVVendedor
 * @property TblKardexcab $tblKardexcab
 * @property TblVFormapago $tblVFormapago
 * @property TblVTipoformapago $tblVTipoformapago
 * @property VentaDet[] $ventaDets
 */
class VentaCab extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_ventacab';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'ven_id';

    /**
     * @var array
     */
    protected $fillable = ['ven_cli_id', 'ven_vend_id', 'ven_tfp_id', 'ven_fp_id', 'ven_kar_id', 'ven_neg_id', 'ven_fecha_registro', 'ven_total', 'ven_usuario_registro'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblCliente()
    {
        return $this->belongsTo('App\TblCliente', 'ven_cli_id', 'cli_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'ven_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblVVendedor()
    {
        return $this->belongsTo('App\TblVVendedor', 'ven_vend_id', 'vend_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblKardexcab()
    {
        return $this->belongsTo('App\TblKardexcab', 'ven_kar_id', 'kar_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblVFormapago()
    {
        return $this->belongsTo('App\TblVFormapago', 'ven_fp_id', 'fp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblVTipoformapago()
    {
        return $this->belongsTo('App\TblVTipoformapago', 'ven_tfp_id', 'tfp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ventaDets()
    {
        // return $this->hasMany('App\Models\VentaDet', 'vd_ven_id', 'ven_id');
        return $this->hasMany(\App\Models\VentaDet::class, 'vd_ven_id', 'ven_id');
    }
}
