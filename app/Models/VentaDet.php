<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $vd_id
 * @property int $vd_ven_id
 * @property int $vd_paq_id
 * @property int $vd_cantidad
 * @property float $vd_total_det
 * @property int $vd_bonificacion
 * @property TblVentacab $ventaCab
 * @property TblPaquete $tblPaquete
 */
class VentaDet extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_ventadet';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'vd_id';

    /**
     * @var array
     */
    protected $fillable = ['vd_ven_id', 'vd_paq_id', 'vd_cantidad', 'vd_total_det','vd_bonificacion'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ventaCab()
    {
        // return $this->belongsTo('App\Models\VentaCab', 'vd_ven_id', 'ven_id');
        return $this->belongsTo(\App\Models\VentaCab::class,'vd_ven_id', 'ven_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblPaquete()
    {
        return $this->belongsTo('App\TblPaquete', 'vd_paq_id', 'paq_id');
    }
}
