<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $tfp_id
 * @property int $tfp_neg_id
 * @property string $tfp_nombre
 * @property string $tfp_descripcion
 * @property string $tfp_estado
 * @property TblNegocio $tblNegocio
 * @property TblVentacab[] $tblVentacabs
 */
class TipoFormaPago extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_v_tipoformapago';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'tfp_id';

    /**
     * @var array
     */
    protected $fillable = ['tfp_neg_id', 'tfp_nombre', 'tfp_descripcion', 'tfp_estado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'tfp_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblVentacabs()
    {
        return $this->hasMany('App\TblVentacab', 'ven_fpag_id', 'tfp_id');
    }
}
