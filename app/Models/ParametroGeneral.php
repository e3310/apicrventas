<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $par_id
 * @property string $par_nombre
 * @property boolean $par_estado
 * @property string $par_valor
 */
class ParametroGeneral extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_parametrosgenerales';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'par_id';

    /**
     * @var array
     */
    protected $fillable = ['par_nombre', 'par_estado', 'par_valor'];

}
