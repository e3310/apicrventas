<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $tipo_id
 * @property string $tipo_nombre
 * @property string $tipo_descripcion
 * @property TblPermisopagina[] $tblPermisopaginas
 * @property TblUsuario[] $tblUsuarios
 */
class TipoUsuario extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_tipousuario';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'tipo_id';

    /**
     * @var array
     */
    protected $fillable = ['tipo_nombre', 'tipo_descripcion'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblPermisopaginas()
    {
        return $this->hasMany('App\TblPermisopagina', 'per_tipo_us', 'tipo_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblUsuarios()
    {
        return $this->hasMany('App\TblUsuario', 'us_tipo_id', 'tipo_id');
    }
}
