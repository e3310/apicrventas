<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $pro_id
 * @property int $pro_neg_id
 * @property string $pro_nombre
 * @property float $pro_costo
 * @property float $pro_precio
 * @property int $pro_stock
 * @property string $pro_estado
 * @property TblNegocio $tblNegocio
 * @property TblCompra[] $tblCompras
 * @property TblKardexdet[] $tblKardexdets
 * @property TblPaquete[] $tblPaquetes
 */
class Producto extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_producto';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'pro_id';

    /**
     * @var array
     */
    protected $fillable = ['pro_neg_id', 'pro_nombre', 'pro_costo','pro_precio','pro_stock', 'pro_estado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'pro_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblCompras()
    {
        return $this->hasMany('App\TblCompra', 'com_pro_id', 'pro_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblKardexdets()
    {
        return $this->hasMany('App\TblKardexdet', 'kd_pro_id', 'pro_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblPaquetes()
    {
        return $this->hasMany('App\TblPaquete', 'paq_prod_id', 'pro_id');
    }
}
