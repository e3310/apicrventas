<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $kar_id
 * @property string $kar_tipo_mov
 * @property int $kar_neg_id
 * @property string $kar_estado
 * @property string $kar_fecha_registro
 * @property TblMovimiento $tblMovimiento
 * @property TblNegocio $tblNegocio
 * @property KardexDet[] $kardexDets
 * @property TblVentacab[] $tblVentacabs
 */
class KardexCab extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_kardexcab';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'kar_id';

    /**
     * @var array
     */
    protected $fillable = ['kar_tipo_mov', 'kar_neg_id', 'kar_fecha_registro'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblMovimiento()
    {
        return $this->belongsTo('App\TblMovimiento', 'kar_tipo_mov', 'mov_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'kar_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kardexDets()
    {
        // return $this->hasMany('App\TblKardexdet', 'kd_kar_id', 'kar_id');
        return $this->hasMany(\App\Models\KardexDet::class, 'kd_kar_id', 'kar_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblVentacabs()
    {
        return $this->hasMany('App\TblVentacab', 'ven_kar_id', 'kar_id');
    }

    // public function __construct(array $attributes = array())
    // {
    //     parent::__construct($attributes);
    // }
}
