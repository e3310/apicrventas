<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $pais_id
 * @property int $pais_neg_id
 * @property string $pais_nombre
 * @property string $pais_estado
 * @property TblNegocio $tblNegocio
 * @property TblCliente[] $tblClientes
 */
class Pais extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_pais';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'pais_id';

    /**
     * @var array
     */
    // protected $fillable = ['pais_neg_id', 'pais_nombre', 'pais_estado'];
    protected $fillable = ['pais_neg_id', 'pais_nombre'];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'pais_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblClientes()
    {
        return $this->hasMany('App\TblCliente', 'cli_pais_id', 'pais_id');
    }
}
