<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $kd_id
 * @property int $kd_kar_id
 * @property int $kd_pro_id
 * @property int $kd_cantidad
 * @property float $kd_total_detalle
 * @property int $kd_stock
 * @property float $kd_costo
 * @property float $kd_costo_stock
 * @property KardexCab $kardexCab
 * @property TblProducto $tblProducto
 */
class KardexDet extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_kardexdet';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'kd_id';

    /**
     * @var array
     */
    protected $fillable = ['kd_kar_id', 'kd_pro_id', 'kd_cantidad', 'kd_total_detalle', 'kd_stock', 'kd_costo'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kardexCab()
    {
        return $this->belongsTo(\App\Models\KardexCab::class, 'kd_kar_id', 'kar_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblProducto()
    {
        return $this->belongsTo('App\TblProducto', 'kd_pro_id', 'pro_id');
    }
}
