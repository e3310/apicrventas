<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $trb_id
 * @property int $trb_fp_id
 * @property int $trb_neg_id
 * @property float $trb_total_transferencia
 * @property float $trb_costo_transferencia
 * @property float $trb_total_transferido
 * @property string $trb_fecha_transferencia
 * @property string $trb_fecha_registro
 * @property int $trb_usuario_registro
 * @property string $trb_estado
 * @property TblVFormapago $tblVFormapago
 * @property TblNegocio $tblNegocio
 */
class TransferenciaBancaria extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_transferenciabancaria';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'trb_id';

    /**
     * @var array
     */
    protected $fillable = ['trb_fp_id', 'trb_neg_id', 'trb_total_transferencia', 'trb_costo_transferencia', 'trb_total_transferido', 'trb_fecha_transferencia', 'trb_fecha_registro', 'trb_usuario_registro','trb_estado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblVFormapago()
    {
        return $this->belongsTo('App\TblVFormapago', 'trb_fp_id', 'fp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'trb_neg_id', 'neg_id');
    }
}
