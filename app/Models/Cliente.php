<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $cli_id
 * @property int $cli_pais_id
 * @property int $cli_neg_id
 * @property string $cli_usuario_credito
 * @property string $cli_nombres
 * @property string $cli_apellidos
 * @property string $cli_telefono
 * @property string $cli_ci
 * @property string $cli_ciudad
 * @property string $cli_direccion
 * @property string $cli_email
 * @property string $cli_genero
 * @property string $cli_estado
 * @property int $cli_usuario_registro
 * @property string $cli_fecha_registro
 * @property TblNegocio $tblNegocio
 * @property TblPai $tblPai
 * @property TblCliventasme[] $tblCliventasmes
 * @property TblVentacab[] $tblVentacabs
 */
class Cliente extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_cliente';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'cli_id';

    /**
     * @var array
     */
    protected $fillable = ['cli_pais_id', 'cli_neg_id', 'cli_usuario_credito', 'cli_nombres', 'cli_apellidos', 'cli_telefono', 'cli_ci', 'cli_ciudad', 'cli_direccion', 'cli_email', 'cli_genero', 'cli_estado', 'cli_usuario_registro', 'cli_fecha_registro'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'cli_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblPai()
    {
        return $this->belongsTo('App\TblPai', 'cli_pais_id', 'pais_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblCliventasmes()
    {
        return $this->hasMany('App\TblCliventasme', 'vtam_cli_id', 'cli_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblVentacabs()
    {
        return $this->hasMany('App\TblVentacab', 'ven_cli_id', 'cli_id');
    }
}
