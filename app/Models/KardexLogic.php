<?php

namespace App\Models;

use App\Models\Producto;
use App\Models\KardexCab;
use App\Models\KardexDet;
use App\Models\Compra;
use App\Models\VentaCab;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class KardexLogic
{
    /**
     * OPERACIONES PRODUCTO
     */

    /**
     * Obtiene información de único producto en base de datos
     */
    public function getProductoCredito(){
        $producto = Producto::where('pro_nombre','CREDITO')->first();
        if(!is_null($producto)){
            return $producto;
        }else{
            //crearProducto en lugar de lanzar la excepcion
            throw new HttpException(204,"Producto no actualizado");
        }
    }

    public function getProductoNegocio(int $idNegocio){
        $producto = Producto::where('pro_nombre','CREDITO')
        ->where('pro_neg_id', $idNegocio)->first();
        if(!is_null($producto)){
            return $producto;
        }else{
            //crearProducto en lugar de lanzar la excepcion
            throw new HttpException(204,"Producto no actualizado");
        }
    }

    /**
     * Actualiza stock en tabla de producto
     */
    public function updateStockCostoProducto(int $idProducto,int $negId, 
    int $stockNuevo, float $costo = 0){
        try {
            $arrayUpdate =[];
            $arrayUpdate["pro_stock"] = $stockNuevo;
            if($costo >0){
                $arrayUpdate+= ["pro_costo" => $costo];
            }
            $updateProducto = Producto::where('pro_id', $idProducto)
            ->where("pro_neg_id",$negId)
            ->update($arrayUpdate);
        } catch (\Illuminate\Database\QueryException $e) {
            throw new HttpException(304,"Producto no actualizado");
        } catch (\Exception $e) {
            throw new HttpException(304,"Producto no actualizado");
        }
        // if($updateProducto){
        //     throw new HttpException(304,"Producto no actualizado");
        // }
    }

    public function getPaquete(int $idPaquete){
        $paquete = Paquete::find($idPaquete);
        if(!is_null($paquete)){
            return $paquete;
        }else{
            throw new HttpException(204,"Paquete no encontrado");
        }
    }

    /**
     * OPERACIONES KARDEX
     */

    public function createKardexCab(string $tipoOperacion, int $idNegocio){
        $kardexCab = KardexCab::create([
            'kar_tipo_mov' => $tipoOperacion,
            'kar_neg_id'=> $idNegocio
        ]);
        return $kardexCab;
     }

     public function createKardexDet(int $cabId, Producto $producto, int $cantidad, 
     int $stockNuevo,string $tipoOperacion, float $nuevoCosto=0){
        $nuevaCantidad = $cantidad;
        if($tipoOperacion === "V"){
            $nuevaCantidad = $nuevaCantidad*(-1);
        }

        $costo = $producto->pro_costo;
        if($tipoOperacion === "C" && $nuevoCosto>0){
            $costo= round((($producto->pro_costo * $producto->pro_stock)
                +($nuevaCantidad * $nuevoCosto))
                /($nuevaCantidad+$producto->pro_stock),2);
        }
        $totalDetalle = abs($nuevaCantidad) * $costo;

        $detalleKardex = new KardexDet(["kd_kar_id"=>$cabId,
        "kd_pro_id"=>$producto->pro_id,
        "kd_cantidad"=> $nuevaCantidad,
        "kd_costo" => $costo,
        "kd_stock" => $stockNuevo,
        "kd_total_detalle" => $totalDetalle ]);
        return $detalleKardex;
     }

     //RECALCULO KARDEX

    /**
     * Obtiene el id del registro anterior de kardex
     */
     public function getIdKardexPrev(int $idCurrentKardex, int $negId){
        $anteriorKardex=KardexCab::where('kar_id','<',$idCurrentKardex)
        ->where('kar_estado','A')
        ->where('kar_neg_id',$negId)
        ->orderBy('kar_fecha_registro','desc')->first();
        if(!is_null($anteriorKardex)){
            return $anteriorKardex->kar_id;
        }else{
            throw new HttpException(204,"Registro kardex no encontrado");
        }
     }

     /**
      * Obtiene el objeto de filas de kardex registrados posterior
      * al movimiento actual de kardex, incluido
      */
    public function getRowsKardexNext(int $idPrevKardex,int $negId){
        $rowsError= KardexCab::where('kar_id','>=',$idPrevKardex)
        ->where('kar_estado','A')
        ->where('kar_neg_id',$negId);
        return $rowsError;
    }

    /**
     * Obtener movimientos de kardex partiendo de una coleccion
     */
    public function getMovsKardexNext($rowsError){
        $movs = $rowsError->get();
        if(!is_null($movs)){
            return $movs;
        }else{
            throw new HttpException(204,"Movs kardex no encontrado");
        }
    }

    /**
     * Obtener el campo kar_id de una lista de objetos de tipo
     * KardexCab
     */
    public function getIdsMovsKardex($movs){
        $ids = [];
        if(!is_null($movs)){
            $ids = $movs->map(function ($item) {
                return $item->kar_id;
              });
            return $ids;
        }else{
            throw new HttpException(204,"Movs kardex no encontrado");
        }
    }

    /**
     * Obtener detalles KardexDet de ids de cabeceras 
     * contenidas en un array int
     */
    public function getDetKardexByIdArray ($ids){
        if($ids->count() >0){
            return KardexDet::whereIn('kd_kar_id', $ids)
                        ->orderBy('kd_id')->get();
        }else{
            throw new HttpException(204,"KardexDet not found");
        }
    }

    /**
     * Obtener movimientos de kardex de un tipo especifico
     */
    public function getKardexOperation($rowsError, string $tipoOperacion){
        $movs= $rowsError->where('kar_tipo_mov', $tipoOperacion)->get();
        if(!is_null($movs)){
            return $movs;
        }else{
            throw new HttpException(204,"Movs not found");
        }
    }

    /**
     * Obtener registro tipo compra por array de ids
     */
    public function getComprasCostoByArray($ids, int $negId){
        $comprasCosto = Compra::whereIn('com_kar_id', $ids)
        ->where('com_estado','A')
        ->where('com_neg_id',$negId)
        ->select('com_id','com_cantidad','com_costo','com_kar_id')->get();
        if(!is_null($comprasCosto)){
            return $comprasCosto;
        }else{
            throw new HttpException(204,"Compras not found");
        }
    }

    public function verifyDateChangeStatus($fecha){
        $initDate = Carbon::now()->subDays(3);
        $endDate = Carbon::parse($fecha)->format('Y-m-d H:i:s');
        if($endDate >=$initDate){
        // if($endDate->greaterThan($initDate)){
            return true;
        }else return false;
    }

    /**
     * Permite cambiar el estado del registro de kardex según su id
     */
    public function changeStatusKardex(int $idKardex, string $newStatus){
        try{
            $kardex = KardexCab::find($idKardex);
            $kardex->kar_estado= $newStatus;
            $savedKardex= $kardex->save();
            if($savedKardex){
                return true;
            }
        } catch (\Illuminate\Database\QueryException $e) {
            throw new HttpException(304,"Producto no actualizado");
        } catch (\Exception $e) {
            throw new HttpException(304,"Producto no actualizado");
        }
    }

    public function regularizarStock($movsRegularizar,$detRegularizar, $comprasCosto){
        $index =0;                
        foreach($detRegularizar as $det){
            if($index>0){
                $anteriorKardex = $detRegularizar[$index-1];                        
                $actualKardex = $det;
                $costoNuevo= $anteriorKardex->kd_costo;
                $stockNuevo = $anteriorKardex->kd_stock + (abs($actualKardex->kd_cantidad)*-1);
                $cab = $movsRegularizar->where('kar_id', $det->kd_kar_id)->first();
                if(strcmp($cab["kar_tipo_mov"], "C") === 0){
                    $buscarCompra= $comprasCosto->where('com_kar_id',$det->kd_kar_id)->first();
                    $costoNuevo= round(
                        (($anteriorKardex->kd_stock * $anteriorKardex->kd_costo) 
                        + ($actualKardex->kd_cantidad * $buscarCompra->com_costo))
                        /($anteriorKardex->kd_stock+abs($actualKardex->kd_cantidad)),2);
                        $stockNuevo = $anteriorKardex->kd_stock + ($actualKardex->kd_cantidad);
                }
                $totalDetalle = round($actualKardex->kd_cantidad * $costoNuevo,2);
                $updated = KardexDet::where('kd_kar_id',$det->kd_kar_id)
                ->update(['kd_stock' => $stockNuevo,
                    'kd_costo'=> $costoNuevo,
                    'kd_total_detalle'=> round(($det->kd_cantidad * $costoNuevo),2)]);
                //Modificando coleccion
                $det->kd_stock = $stockNuevo;
                $det->kd_costo= $costoNuevo;                         
            }
            $index++;
        }
    }

    public function recalculoKardex(int $idKardexAnterior, int $idNegocio){
        try{
            $idAnteriorKardex = $this->getIdKardexPrev($idKardexAnterior, $idNegocio);
            $rowsError = $this->getRowsKardexNext($idAnteriorKardex,$idNegocio);
            $movsRegularizar = $this->getMovsKardexNext($rowsError);
            //KARDEX DET
            $idsRegularizar = $this->getIdsMovsKardex($movsRegularizar);
            $detRegularizar = $this->getDetKardexByIdArray($idsRegularizar);
            //COMPRAS
            $movsCompras = $this->getKardexOperation($rowsError, 'C');
            $idsKarCompras = $this->getIdsMovsKardex($movsCompras);
            $comprasCosto = $this->getComprasCostoByArray($idsKarCompras,$idNegocio);             

            $this->regularizarStock($movsRegularizar,$detRegularizar,$comprasCosto);
            
            $ultimoKardex= $detRegularizar[$detRegularizar->count()-1];
            $this->updateStockCostoProducto($ultimoKardex->kd_pro_id,
            $idNegocio, $ultimoKardex->kd_stock, $ultimoKardex->kd_costo);
            return true;
        }catch (\Exception $e) {
            throw new HttpException(404,"error");
            return false;
        }
    }

    public function idKardexPrev($fecha){
        return KardexCab::where("kar_fecha_registro","<",$fecha)
        ->orderBy("kar_fecha_registro","desc")->first();
    }

    public function verifyStock(int $cantidadVenta, int $idNegocio){
        $compare = ["available" => false, "stock" => null];
        $lastDetKardex= KardexCab::join("tbl_kardexdet","tbl_kardexcab.kar_id","=","tbl_kardexdet.kd_kar_id")
        ->where("kar_estado","A")->where("kar_neg_id",$idNegocio)
        ->orderBy('kd_id', 'desc')->orderBy('kar_fecha_registro')->select('kd_stock')
        ->first();
        if(!is_null($lastDetKardex)){
            if($lastDetKardex->kd_stock>=$cantidadVenta){
                $compare= ["available" => true, "stock"=>$lastDetKardex->kd_stock ];
            }else{
                $compare["stock"] = $lastDetKardex->kd_stock;
            }
        }
        return $compare;
    }











}