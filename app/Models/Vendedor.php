<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $vend_id
 * @property int $vend_per_id
 * @property int $vend_neg_id
 * @property string $vend_estado
 * @property int $vend_usuario_registro
 * @property string $vend_fecha_registro
 * @property TblNegocio $tblNegocio
 * @property TblPersona $tblPersona
 */
class Vendedor extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_v_vendedor';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'vend_id';

    /**
     * @var array
     */
    protected $fillable = ['vend_per_id', 'vend_neg_id', 'vend_estado', 'vend_usuario_registro', 'vend_fecha_registro'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'vend_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblPersona()
    {
        return $this->belongsTo('App\TblPersona', 'vend_per_id', 'per_id');
    }
}
