<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $com_id
 * @property int $com_pro_id
 * @property int $com_neg_id
 * @property int $com_cantidad
 * @property float $com_costo
 * @property float $com_total
 * @property string $com_estado
 * @property int $com_kar_id
 * @property string $com_fecha_registro
 * @property int $com_usuario_registro
 * @property TblProducto $tblProducto
 * @property TblNegocio $tblNegocio
 * @property TblKardexcab[] $tblKardexcabs
 */
class Compra extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_compra';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'com_id';

    /**
     * @var array
     */
    protected $fillable = ['com_pro_id', 'com_neg_id', 'com_cantidad', 'com_costo', 'com_total','com_estado', 'com_fecha_registro', 'com_usuario_registro','com_kar_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblProducto()
    {
        return $this->belongsTo('App\TblProducto', 'com_pro_id', 'pro_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'com_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblKardexcabs()
    {
        return $this->hasMany('App\TblKardexcab', 'kar_com_id', 'com_id');
    }
}
