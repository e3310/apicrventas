<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $rcvc_id
 * @property int $rcvc_neg_id
 * @property string $rcvc_tipo
 * @property string $rcvc_mes
 * @property string $rcvc_anio
 * @property string $rcvc_fecha_inicio
 * @property string $rcvc_fecha_fin
 * @property string $rcvc_fecha_generado
 * @property TblNegocio $tblNegocio
 * @property RepCliVentaDet[] $repCliVentaDets
 */
class RepCliVentaCab extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_rep_cliventacab';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'rcvc_id';

    /**
     * @var array
     */
    protected $fillable = ['rcvc_neg_id', 'rcvc_tipo', 'rcvc_mes', 'rcvc_anio', 'rcvc_fecha_inicio', 'rcvc_fecha_fin', 'rcvc_fecha_generado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'rcvc_neg_id', 'neg_id');
    }

        /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function repCliVentaDets()
    {
        return $this->hasMany(\App\Models\RepCliVentaDet::class, 'rcvd_rcvc_id', 'rcvc_id');
    }
}
