<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $per_id
 * @property string $per_cedula
 * @property string $per_nombres
 * @property string $per_apellidos
 * @property string $per_genero
 * @property string $per_telefono
 * @property string $per_email
 * @property string $per_direccion
 * @property string $per_ciudad
 * @property string $per_pais
 * @property TblUsuario[] $tblUsuarios
 * @property TblVendedor[] $tblVendedors
 */
class Persona extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_persona';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'per_id';

    /**
     * @var array
     */
    protected $fillable = ['per_cedula', 'per_nombres', 'per_apellidos', 'per_genero', 'per_telefono', 'per_email', 'per_direccion', 'per_ciudad', 'per_pais'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblUsuario()
    {
        return $this->hasOne(Usuario::class, 'us_per_id', 'per_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblVendedor()
    {
        return $this->hasMany('App\TblVendedor', 'vend_per_id', 'per_id');
    }
}
