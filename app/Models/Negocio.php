<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $neg_id
 * @property string $neg_nombre
 * @property string $neg_estado
 * @property int $neg_usuario_registro
 * @property string $neg_fecha_registro
 * @property string $neg_logo
 * @property TblCliente[] $tblClientes
 * @property TblCompra[] $tblCompras
 * @property TblCuentabancarium[] $tblCuentabancarias
 * @property TblFormapago[] $tblFormapagos
 * @property TblKardexcab[] $tblKardexcabs
 * @property TblPai[] $tblPais
 * @property TblPaquete[] $tblPaquetes
 * @property TblProducto[] $tblProductos
 * @property TblTransferenciabancarium[] $tblTransferenciabancarias
 * @property TblVendedor[] $tblVendedors
 * @property TblVentacab[] $tblVentacabs
 */
class Negocio extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_negocio';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'neg_id';

    /**
     * @var array
     */
    protected $fillable = ['neg_nombre', 'neg_estado', 'neg_usuario_registro', 'neg_fecha_registro', 'neg_logo'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblClientes()
    {
        return $this->hasMany('App\TblCliente', 'cli_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblCompras()
    {
        return $this->hasMany('App\TblCompra', 'com_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblCuentabancarias()
    {
        return $this->hasMany('App\TblCuentabancarium', 'ctab_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblFormapagos()
    {
        return $this->hasMany('App\TblFormapago', 'fpag_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblKardexcabs()
    {
        return $this->hasMany('App\TblKardexcab', 'kar_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblPais()
    {
        return $this->hasMany('App\TblPai', 'pais_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblPaquetes()
    {
        return $this->hasMany('App\TblPaquete', 'paq_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblProductos()
    {
        return $this->hasMany('App\TblProducto', 'pro_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblTransferenciabancarias()
    {
        return $this->hasMany('App\TblTransferenciabancarium', 'trb_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblVendedors()
    {
        return $this->hasMany('App\TblVendedor', 'vend_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblVentacabs()
    {
        return $this->hasMany('App\TblVentacab', 'ven_neg_id', 'neg_id');
    }
}
