<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $fp_id
 * @property int $fp_tipofp
 * @property int $fp_neg_id
 * @property string $fp_numero
 * @property string $fp_tipo_cuenta
 * @property float $fp_saldo
 * @property string $fp_entidad_bancaria
 * @property string $fp_nombres_titular
 * @property string $fp_apellidos_titular
 * @property string $fp_ci_titular
 * @property string $fp_telefono_titular
 * @property string $fp_email_titular
 * @property string $fp_estado
 * @property int $fp_usuario_registro
 * @property string $fp_fecha_registro
 * @property TblNegocio $tblNegocio
 * @property TblVTipoformapago $tblVTipoformapago
 * @property TblTransferenciabancarium[] $tblTransferenciabancarias
 * @property TblVentacab[] $tblVentacabs
 */
class FormaPago extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tbl_v_formapago';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'fp_id';

    /**
     * @var array
     */
    protected $fillable = ['fp_tipofp', 'fp_neg_id', 'fp_numero', 'fp_tipo_cuenta', 'fp_saldo', 'fp_entidad_bancaria', 'fp_nombres_titular', 'fp_apellidos_titular', 'fp_ci_titular', 'fp_telefono_titular', 'fp_email_titular', 'fp_estado', 'fp_usuario_registro', 'fp_fecha_registro'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblNegocio()
    {
        return $this->belongsTo('App\TblNegocio', 'fp_neg_id', 'neg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tblVTipoformapago()
    {
        return $this->belongsTo('App\TblVTipoformapago', 'fp_tipofp', 'tfp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblTransferenciabancarias()
    {
        return $this->hasMany('App\TblTransferenciabancarium', 'trb_ctab_id', 'fp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tblVentacabs()
    {
        return $this->hasMany('App\TblVentacab', 'ven_ctab_id', 'fp_id');
    }
}
