<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ParametroGeneral;

class ParametroGeneralLogic extends Model
{
    // use HasFactory;
    public function createParametro(string $nombre, string $valor = null){
        $parametro = ParametroGeneral::updateOrCreate([
            'par_nombre' => $nombre,
            'par_valor'=> $valor
        ]);
        return $parametro;
    }

    public function findParametro(string $nombre){
        $parametro = KardexCab::where('par_nombre',$nombre)->first();
        return $parametro;
    }
}
