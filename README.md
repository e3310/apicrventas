<h3>API de sistema de gestión de ventas</h3>
<p>Funcionalidades incluidas</p>
<ul>
    <li>Módulo de registro de clientes</li>
    <li>Módulo de registro de formas de pago</li>
    <li>Módulo de cuentas bancarias para control de saldos</li>
    <li>Módulo de control de stock de productos</li>
    <li>Módulo de ventas</li>
</ul>
