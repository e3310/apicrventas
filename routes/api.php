<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\NegocioController;
use App\Http\Controllers\Api\PaisController;
use App\Http\Controllers\Api\ClienteController;
use App\Http\Controllers\Api\ProductoController;
use App\Http\Controllers\Api\PaqueteController;
use App\Http\Controllers\Api\CompraController;
use App\Http\Controllers\Api\TipoFormaPagoController;
use App\Http\Controllers\Api\FormaPagoController;
use App\Http\Controllers\Api\TransferenciaBancariaController;
use App\Http\Controllers\Api\UsuarioController;
use App\Http\Controllers\Api\PersonaController;
use App\Http\Controllers\Api\VendedorController;
use App\Http\Controllers\Api\VentaController;
use App\Http\Controllers\Api\KardexController;
use App\Http\Controllers\Api\TipoUsuarioController;
use App\Http\Controllers\Api\RepClienteVentaController;

use App\Http\Controllers\Api\LoginController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::middleware(['auth:sanctum'])->group(function () {
    /**
     * API Negocio
     */
    Route::apiResource('negocios', NegocioController::class)
    ->only(['index','show', 'store']);
    Route::put('/negocios/cambiarEstado/{id}', [NegocioController::class,'cambiarEstado']);
    Route::put('/negocios/{id}',[NegocioController::class,'update']);

    /**
     * API Pais
     */
    Route::apiResource('paises', PaisController::class)
    ->only(['index','store']);
    Route::put('/paises/cambiarEstado/{id}', [PaisController::class,'cambiarEstado']);
    Route::put('/paises/{id}',[PaisController::class,'update']);
    Route::get('/paises/activos',[PaisController::class,'getActivos']);

    /**
     * API Cliente
     */
    Route::apiResource('clientes', ClienteController::class)
    ->only(['show', 'store']);
    Route::get('/clientes/all/{idNegocio}', [ClienteController::class,'index']);
    Route::get('/clientes/activos/{idNegocio}', [ClienteController::class,'getActivos']);
    Route::post('/clientes/search', [ClienteController::class,'search']);
    Route::put('/clientes/cambiarEstado/{id}', [ClienteController::class,'cambiarEstado']);
    Route::put('/clientes/{id}',[ClienteController::class,'update']);
    Route::post('/clientes/masivo', [ClienteController::class,'storeMasivo']);

    /**
     * API Producto
     */
    Route::apiResource('productos', ProductoController::class)
    ->only(['store']);
    Route::get('/productos/all/{idNegocio}', [ProductoController::class,'index']);
    Route::get('/productos/count/{idNegocio}',[ProductoController::class,'count']);

    /**
     * API Paquete
     */
    Route::apiResource('paquetes', PaqueteController::class)
    ->only(['store']);
    Route::get('/paquetes/all/{idNegocio}', [PaqueteController::class,'index']);
    Route::put('/paquetes/cambiarEstado/{id}', [PaqueteController::class,'cambiarEstado']);
    Route::put('/paquetes/{id}',[PaqueteController::class,'update']);
    Route::get('/paquetes/activos/{idNegocio}',[PaqueteController::class,'getActivos']);

    /**
     * API Tipo Formas de pago
     */
    Route::apiResource('tiposFPago', TipoFormaPagoController::class)
    ->only(['store']);
    Route::get('/tiposFPago/all/{idNegocio}', [TipoFormaPagoController::class,'index']);
    Route::put('/tiposFPago/cambiarEstado/{id}', [TipoFormaPagoController::class,'cambiarEstado']);
    Route::put('/tiposFPago/{id}',[TipoFormaPagoController::class,'update']);
    Route::get('/tiposFPago/activos/{idNegocio}',[TipoFormaPagoController::class,'getActivos']);

    /**
     * API Forma pago
     */
    Route::apiResource('formasPago', FormaPagoController::class)
    ->only(['store']);
    Route::get('/formasPago/all/{idNegocio}', [FormaPagoController::class,'index']);
    Route::put('/formasPago/cambiarEstado/{id}', [FormaPagoController::class,'cambiarEstado']);
    Route::put('/formasPago/{id}',[FormaPagoController::class,'update']);
    Route::get('/formasPago/activos/{idNegocio}/{idTipo}',[FormaPagoController::class,'getActivos']);

    /**
     * API Transferencias
     */
    Route::apiResource('transferencias', TransferenciaBancariaController::class)
    ->only(['store']);
    Route::get('/transferencias/all/{idNegocio}', [TransferenciaBancariaController::class,'index']);
    Route::put('/transferencias/cambiarEstado/{id}', [TransferenciaBancariaController::class,'cambiarEstado']);
    Route::put('/transferencias/{id}',[TransferenciaBancariaController::class,'update']);
    Route::post('/transferencias/search', [TransferenciaBancariaController::class,'search']);

    /**
     * API User
     */
    // Route::apiResource('users', UsuarioController::class)
    // ->only(['store']);
    Route::get('/users/allAdmin', [UsuarioController::class,'indexAdmin']);
    Route::get('/users/allVendedor/{idNegocio}', [UsuarioController::class,'indexVendedor']);
    Route::post('/users/crearAdmin', [UsuarioController::class,'storeAdmin']);
    Route::post('/users/crearVendedor', [UsuarioController::class,'storeVendedor']);
    Route::put('/users/cambiarEstado/{id}', [UsuarioController::class,'cambiarEstado']);
    // Route::put('/users/cambiarPassword', [UsuarioController::class,'updatePassword']);
    Route::put('/users/cambiarTipo/{id}', [UsuarioController::class,'updateTipo']);

    /**
     * API Persona
     */
    Route::put('/person/{id}',[PersonaController::class,'update']);

    /**
     * API Vendedor
     */
    Route::get('/vendors/activos/{idNegocio}', [VendedorController::class,'getActivos']);
    Route::put('/vendors/cambiarEstado/{idPersona}', [VendedorController::class,'cambiarEstado']);

    /**
     * API Venta
     */
    Route::apiResource('sales', VentaController::class)
    ->only(['store']);
    Route::get('/sales/all/{idNegocio}', [VentaController::class,'index']);
    Route::post('/sales/search', [VentaController::class,'search']);
    Route::put('/sales/cambiarEstado/{id}', [VentaController::class,'cambiarEstado']);

    /**
     * API Compra
     */
    Route::apiResource('purchases', CompraController::class)
    ->only(['store']);
    Route::get('/purchases/all/{idNegocio}', [CompraController::class,'index']);
    Route::post('/purchases/search', [CompraController::class,'search']);
    Route::put('/purchases/cambiarEstado/{id}', [CompraController::class,'cambiarEstado']);

    /**
     * API Kardex
     */
    Route::get('/kardex/all/{idNegocio}', [KardexController::class,'index']);
    Route::post('/kardex/search', [KardexController::class,'search']);
    Route::post('/kardex/show', [KardexController::class,'show']);
    Route::put('/kardex', [KardexController::class,'update']);

    /**
     * API Reporte Ventas mes RepClienteVentaController
     */
    Route::apiResource('repsales', RepClienteVentaController::class)
    ->only(['store']);
    Route::get('/repsales/all/{idNegocio}', [RepClienteVentaController::class,'index']);
    Route::get('/repsales/show/{id}', [RepClienteVentaController::class,'show']);
    Route::put('/repsales',[RepClienteVentaController::class,'update']);


    /**
     * API TipoUsuario
     */
    Route::get('/typeUser/admin',[TipoUsuarioController::class,'showTipoAdmin']);
    Route::get('/typeUser/vendor',[TipoUsuarioController::class,'showTipoVendor']);

    /**
     * API Login
     */
    Route::post('logout', [LoginController::class,'logout']);
});
Route::post('login', [LoginController::class,'login']);
Route::get('/vendors/show/{idUsuario}', [VendedorController::class,'show']);
Route::put('/users/activarPendiente', [UsuarioController::class,'activarPendiente']);
Route::put('/users/cambiarPassword', [UsuarioController::class,'updatePassword']);

